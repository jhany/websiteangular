// Angular
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/router';
import '@angular/forms';
import '@angular/material';

import "@angular/flex-layout";
import 'hammerjs';
import 'elasticsearch-browser';
import 'angular2-fontawesome';

// RxJS
import 'rxjs';

// Other vendors for example jQuery, Lodash or Bootstrap
// You can import js, ts, css, sass, ...
