// globals.ts
import { Injectable } from '@angular/core';

@Injectable()
export class Globals {
  url: string = 'https://newfeature.cominlabs.u-bretagneloire.fr';
  ip: string = 'https://alphadatascience.irisa.fr';
  index_pub : string ="publications_inria";
  doc_type_pub: string ="_doc";
  index_team: string ="nested_researchteam_inria";
  doc_type_team: string ="_doc";
  index_author: string ="nested_inria_author";
  doc_type_author: string ="_doc";

}
