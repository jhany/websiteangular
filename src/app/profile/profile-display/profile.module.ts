import { NgModule }  from '@angular/core';
import { CommonModule }  from '@angular/common';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from "@angular/flex-layout";

import { SharedModule }       from '../shared.module';
import {SharedProfileModule} from '../sharedprofile.module';

import { Profile } from './profile';
import {ProfileRoutingModule} from './profile-routing.module'

//providers
import {SearchIndivService} from '../services/es-indiv-serv';

@NgModule({
  imports: [CommonModule, SharedModule, SharedProfileModule, ProfileRoutingModule, MaterialModule, FormsModule, FlexLayoutModule],
  declarations: [ Profile],
  providers : [ SearchIndivService],
})

export class ProfileModule {}
