import {Component, OnChanges, SimpleChange, OnInit, Inject, Input} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {MatDialogRef} from '@angular/material';
import {MatDialog} from '@angular/material';
import {CleanWords} from '../../utils/cleanwords.utils';
import {AffiliationUtils} from '../../utils/affiliation.utils';
import {SearchIndivService} from '../../services/es-indiv-serv';
import {MakeComment} from '../../template-page/make-comment/makeComment';

/*
*    Component to display publications in list
*/
@Component({
    selector : 'profile-overview',
    templateUrl : './profile-overview.html'
})

/**
* Use to show a user profile with all informations
*/
export class ProfileOverview implements OnInit{

    @Input() resp;
    pageSize = {count: 10, label: '10 per page '};
    currentPage = 1;
    resultStat;

    //Top keywords User
    words = [];
    done = false;

    //Significant keywords
    wordsallkeycloud = [];

    // Main topics
    topics = [];
    doneTopics = false;

    //Graphic
    pubsByYear = [];
    pubscount = false;

    //Clusterings words
    words_lingo = [];
    publications_lingo = {};

    //Can display publication
    currentPublications_done : boolean = false;
    notFound : boolean = false;

    affiliationsUtils = new AffiliationUtils();
    cleanWords = new CleanWords();
    lingo = false;

    private key_user;
    private structure : string = "";
    private fullFormatName : string = "";
    history = [];
    filteredPublications;
    totalItems;
    numPages;
    aff_number;
    fullName = "";
    currentPublications;

    opacity1 = 0.5;
    opacity2 = 1;

    constructor(private route : ActivatedRoute, public dialog : MatDialog, private searchService : SearchIndivService){}

    ngOnInit(){
        this.done = false;
        this.key_user = this.resp._id;
        this.fullName = this.resp._source.formatName;
        this.getUser(this.resp);
    }
    /**
    * Find user and all the information
    */
    getUser(resp){
        let arrayPubs = [];
        arrayPubs = resp._source.pubs;
        this.getUserInfo(resp);
        this.topics = resp._source.main_topics;
        this.getSignificantKeywords(arrayPubs);
    }

    /**
    * Get information for a give user
    * @param resp : response get from elasticsearch
    */
    getUserInfo(resp){
        this.getUserPublications(resp);
        this.getKeywords(resp);
        this.getNumberOfPublicationsByYear(resp);
    }

    /**
    * Get significant keywords for a given user
    * @param id : id of the user
    * @param arrayPubs : list of user's publications
    */
    private getSignificantKeywords(arrayPubs){
        let arrayIdhal = [];
        for(let i = 0; i < arrayPubs.length; i++){
            arrayIdhal.push(arrayPubs[i].halId);
        }
        this.searchService.getSignificantKeywordsAll(arrayIdhal).then(
            resp => {
                this.cleanKeywords(resp);
            }
        )
    }

    /**
    * Get keywords for a user without a key
    * @param id : id of the user
    * @param arrayPubs : list of publications of the user
    */
    private getSignificantKeywordsWithout(id, arrayPubs){
        let arrayIdhal = [];
        for(let i = 0; i < arrayPubs.length; i++){
            arrayIdhal.push(arrayPubs[i].halId);
        }
        this.searchService.getSignificantKeywordsWithoutChronoAll(arrayIdhal).then((resp) => {
            this.cleanKeywords(resp);
        })
    }

    /**
    * Clean keywords for display in world cloud and distance de levenshtein
    * @param resp : response get from the request
    */
    private  cleanKeywords(resp){
        this.done = false;
        let result_fr = resp.aggregations.all_pub.pub_filter.most_sig_words.buckets;
        this.wordsallkeycloud = this.cleanWords.getSignificantKeywords(result_fr, this.key_user);
        // this.words = this.words.concat(this.wordsallkeycloud);
        this.words = this.cleanWords.getCleanLevenshtein(this.words);
        this.words = this.words.slice(0,20);
        this.wordsallkeycloud.sort((a, b) =>{
            return b.size - a.size;
        });
        if(this.wordsallkeycloud[0] != undefined){
            let max_norm = this.wordsallkeycloud[0].size;
            for(let i = 0; i < this.wordsallkeycloud.length; i++){
                this.wordsallkeycloud[i] = {
                    "text": this.wordsallkeycloud[i].text,
                    "size": ((this.wordsallkeycloud[i].size - this.wordsallkeycloud[this.wordsallkeycloud.length - 1].size) / (max_norm - this.wordsallkeycloud[this.wordsallkeycloud.length - 1].size)) * 80,
                    "affiliationId" : this.wordsallkeycloud[i].affiliationId
                };
            }
        }
        this.done = true;
    }

    /**
    * Sort publication by year
    * @param resp : reponse get from the request
    */
    private getUserPublications(resp) {
        let allPublications = resp._source.pubs;
        // Sort publications by year
        allPublications.sort( (a, b) => {
            return b.year - a.year;
        });
        this.filteredPublications = allPublications;
        this.totalItems = allPublications.length;
        this.setCurrentPublications();
        this.resultStat = {total : this.totalItems, took : 0};
        this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
    }

    /**
    * Use to display the graph Number of publications by year
    * @param resp : reponse get from the request
    */
    private getNumberOfPublicationsByYear(resp) {
        let allPublications = resp._source.pubs;
        let mapyear = new Map();
        for(let i = 0; i < allPublications.length; i++ ){
            if(mapyear.has(allPublications[i].year)){
                let nbPubli = mapyear.get(allPublications[i].year);
                nbPubli = nbPubli + 1;
                mapyear.set(allPublications[i].year, nbPubli);
            }else{
                mapyear.set(allPublications[i].year, 1);
            }
        }
        mapyear.forEach((value, key, map) =>
        {
            this.pubsByYear.push({"key" : key, "doc_count" : value});
        });
        this.pubsByYear.sort((a, b) => {
            return a.key - b.key
        });
        this.pubscount = true;
    }

    /**
    * Get the keywords of affiliation publications and sort the by number of apparition
    * @param resp : response get from elastic for this affiliation
    */
    private getKeywords(resp){
        this.words = this.affiliationsUtils.getKeywords(resp, this.key_user);
        this.words = this.cleanWords.getCleanLevenshtein(this.words);
    }
    /**
    * Select page to display for publication
    * @param page : number of the page
    */
    selectPage(page) {
        this.currentPage = page.numPage;
        this.setCurrentPublications();
    };

    /**
    * Select the new user with new select page size option
    * @param pageSize : nb user to display by page
    */
    selectPageSize(pageSize){
        if(this.currentPage * pageSize.pageSize > this.aff_number){
            this.currentPage = Math.ceil(this.aff_number / pageSize.pageSize)
        }
        this.pageSize = pageSize.pageSize;
        this.setCurrentPublications();
    };

    /**
    * Set publications to display
    */
    private setCurrentPublications() {
        this.currentPublications_done = false;
        let from = (this.currentPage - 1) * this.pageSize.count;
        let to = from + this.pageSize.count;
        this.currentPublications = this.filteredPublications.slice(from, to);
        this.totalItems = this.filteredPublications.length;
        this.currentPublications_done = true;
        this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
    }

    /**
    * Check if allPublication exist
    * @returns {boolean}
    */
    isAvailableResults() {
        return this.currentPublications ? true : false;
    };

    /**
    * Check if at least 1 publication is available
    * @returns {boolean}
    */
    isAtLeastOneResult() {
        if (!this.isAvailableResults()) {
            return false;
        }
        return this.currentPublications.length > 0;
    };

    /**
    * Go to the publication of the user
    */
    goToAffiliation(){
        window.open("/team/"+this.structure, '_blank');
    };

    /**
    * Use if the user want to make a comment on the page
    * @param ev : event fire when user clik on the button
    */
    showComment(ev){
        let dial = this.dialog.open(MakeComment);
        dial.afterClosed().subscribe(result => {
            this.searchService.setComment(this.key_user, this.fullName, result);
        });
    };

    /**
    * Go to Lingo article or our article
    **/
    gotoArticle(){
          window.open("https://hal.inria.fr/hal-01739845", '_blank');
    }

    /**
    * Go to Lingo article or our article
    **/
    gotoArticleLingo(){
      window.open("https://link.springer.com/chapter/10.1007/978-3-540-39985-8_37", '_blank');
    }

    getLingo(){
        if(this.lingo){
            this.lingo = false;
            this.opacity1 = 0.5;
            this.opacity2 = 1;
        }else{
            this.lingo = true;
            this.opacity1 = 1;
            this.opacity2 = 0.5;
        }
    }

    /**
    * Use to check if is a mobile or not to display list instead of cards
    * @returns {boolean} : true if is a mobile
    */
    mobileCheck() {
        var check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor);
        return check;
    };

}
