import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'make-comment',
  template: `
  <h3 md-dialog-title class="modal-title" style="background-color: rgb(56, 66, 87);">
      Make a comment
  </h3>
  <mat-form-field>
        <input matInput [(ngModel)]="searchText" placeholder="Make a comment">
  </mat-form-field>
  <mat-dialog-actions fxLayoutAlign="end">
    <button mat-button mat-dialog-close fxLayoutAlign="end">Close</button>
  </mat-dialog-actions>
  <mat-dialog-actions fxLayoutAlign="end">
    <button mat-button [mat-dialog-close]="searchText" >Ok</button>
  </mat-dialog-actions>
  `
})
export class MakeComment {

  searchText;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
  }
}
