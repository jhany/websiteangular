import {Component, OnChanges, SimpleChange, HostListener} from '@angular/core';
import {SearchIndivService} from '../../services/es-indiv-serv';
import {CommentService} from '../../services/comment-serv';
import { MatomoTracker } from 'ngx-matomo';
import {state} from '@angular/animations';

@Component({
    selector : 'all-search',
    templateUrl : './all_search.html',
    styleUrls : ['./all_search.css']
})

export class AllSearch{

    // Variable initialization
    explanationBtn = false;
    isSearchUser = true;
    exactMatch = false;
    exactMatchDone = false;
    allStruct = [];
    searchResphit;
    suggestion_search;
    pubsUser = [];
    suggestion_topic;
    listTopic;
    suggestion_text;
    searchText = "";

    //Use for results info
    pageSize = {count: 10, label: "10 per page"};
    resultStat;

    researchStart = false;
    researchProcess = false;

    teams = true;
    individuals = true;
    publications = true;

    flexTeams = 30;
    flexIndividuals = 30;
    flexPublications = 40;
    list_word2vec;
    list_word2vec_done = false;
    notclick = true;
    private window: Window;
    options = [];
    person = "";
    isaPerson = false;

    constructor(private searchIndiv : SearchIndivService,private commentServ :  CommentService, private matomoTracker: MatomoTracker){
        if(this.mobileCheck() || window.innerWidth < 1600){
            this.individuals = false;
            this.publications = false;
            this.flexTeams = 100;
        }
    }

    @HostListener('document:keydown.enter', ['$event'])
    PressEnter(event: KeyboardEvent) {
        // your click logic
        this.notclick = false;
        this.textSearch(this.searchText)
    }

    @HostListener('document:keyup', ['$event'])
        PressKeywdow(event: KeyboardEvent) {
        this.suggestion(this.searchText)
    }

    @HostListener('document:click', ['$event'])
    documentClick(event: MouseEvent) {
        // your click logic
        // console.log(event.target.id);
        if(event.target["id"] == "button-search")
        {
            this.notclick = false;
            this.textSearch(this.searchText)
        }
    }

    @HostListener('body:resize', ['$event'])
    onResize(event) {
      let width = event.target.innerWidth;
      if(width < 1600){
          this.individuals = false;
          this.publications = false;
          this.flexTeams = 100;
      }
      if(width >= 1600){
          this.teams = true;
          this.individuals = true;
          this.publications = true;
          this.flexTeams = 30;
          this.flexIndividuals = 30;
          this.flexPublications = 40;
      }
    }

    /**
    * Suggest a text when the user insert some letter
    * Not use for now
    * @param text : text insert by the user
    * @returns {*} : list of suggestion
    */
    suggestion(text) {
        if (text.length > 1) {
           let res = this.searchIndiv.suggestion(text).then(
               resp => {
                   let option_to_clean = [];
                   for(let i = 0; i < resp.suggest.text_suggest[0].options.length; i++){
                       if(!option_to_clean.includes(resp.suggest.text_suggest[0].options[i].text.toLowerCase().trim())){
                           option_to_clean.push(resp.suggest.text_suggest[0].options[i].text.toLowerCase().trim())
                       }
                   }
                   this.options = option_to_clean;
               }
           );
           if(text.length > 3){
               let res = this.searchIndiv.getUserMatch(text).then(
                   resp => {
                       let option_to_clean = [];
                       if(resp.hits.hits.length > 0){
                           this.options = ["Did you mean a person ?", "Did you mean a keyword ?"]
                           this.person = text;
                       }
                   }
               );
           }
        } else {
           this.options = [];
           this.exactMatchDone = false;
           this.isaPerson = false;
        }
    };

    /**
    * OptionSearch : user click on a option  in md autocomplete
    * @param text : the option select by user
    */
    OptionSearch(text){
        this.exactMatchDone = false;
        this.isaPerson = false;
        if(text == "Did you mean a person ?"){
            this.searchText = this.person;
            this.notclick = false;
            this.isaPerson = true;
            this.exactMatchDone = true;
        }else if(text == "Did you mean a keyword ?"){
            this.searchText = this.person;
            this.textSearch(this.searchText)
        }else{
            this.textSearch(text)
        }
    }

    /**
    * textSearch : Use to get response of the search like
    * @param text : text use to get the user
    */
    textSearch(text) {
        this.options = [];
        this.isaPerson = false;
        this.notclick = false;
        if (text.trim().length > 0) {
            this.searchText = text.trim();
            this.researchProcess = true;
            this.exactMatch = false;
            this.exactMatchDone = false;
            this.searchResphit = undefined;
            // Get all affiliations who match with the search
            this.suggestion_search = false;
            let name = text.toLowerCase().trim();
            name = name.replace(/é/g, "e");
            name = name.replace(/è/g, "e");
            name = name.replace(/ê/g, "e");
            name = name.replace(/ë/g, "e");
            name = name.replace(/à/g, "a");
            name = name.replace(/ï/g, "i");
            name = name.replace(/î/g, "i");
            name = name.replace(/ô/g, "o");
            name = name.replace(/ç/g, "c");
            name = name.replace(/ü/g, "u");
            name = name.replace(/ù/g, "u");
            name = name.replace(/-/g, " ");
            this.matomoTracker.trackEvent('request', text);
            this.searchIndiv.getUserExactMatch(name).then(
                resp => {
                    if(resp.hits.total.value != 0){
                        this.allStruct = [];
                        let hist = resp.hits.hits[0]._source.history;
                        for(let i =0; i < hist.length; i++){
                            this.allStruct.push(hist[i].structure)
                        }
                        this.pubsUser = resp.hits.hits[0]._source.pubs;
                        this.exactMatch = true;
                        this.suggestion_search = false;
                        this.exactMatchDone = true;
                    }else{
                        if (text.split(" ").length < 5) {
                            this.searchIndiv.getSuggestionWhenUserSearch(text.trim()).then(
                                resp => {
                                    this.suggestion_text = "";
                                    let suggestionElastic = resp.suggest.mySuggest;
                                    for (let i = 0; i < suggestionElastic.length; i++) {
                                        if (suggestionElastic[i].options.length != 0) {
                                            this.suggestion_text += " " + suggestionElastic[i].options[0].text;
                                            this.suggestion_search = true;
                                        } else {
                                            this.suggestion_text += " " + suggestionElastic[i].text;
                                        }
                                    }
                                }
                            );
                        }
                        this.exactMatchDone = true;
                    }
                }
            )
            this.suggestion_topic = false;
            text = text.replace('"', '');
            text = text.replace('"', '');
            this.searchIndiv.getTextTopics(text.trim()).then(
                (resp) => {
                    this.listTopic = "";
                    let relatedTopic = resp.aggregations.keywords_en.buckets;
                    if(relatedTopic.length > 0){
                        for( var i = 1; i < relatedTopic.length; i++){
                            if (i ==  1){
                                this.listTopic = relatedTopic[i].key;
                            }else{
                                this.listTopic = this.listTopic + ", "+ relatedTopic[i].key;
                            }
                        }
                        this.suggestion_topic = true;
                    }
                })
        }
    };

    onChangeTeam() {
        if(this.teams){
            if(!this.mobileCheck() && window.innerWidth >= 1600) {
                if (this.individuals && this.publications) {
                    this.flexTeams = 30;
                } else if (this.individuals || this.publications) {
                    this.flexTeams = 45;
                } else {
                    this.flexTeams = 100;
                }
            }else{
                this.publications = false;
                this.individuals = false;
                this.flexTeams = 100;
            }
        }
        if(this.individuals){
            if(!this.mobileCheck() && window.innerWidth >= 1600) {
                if (this.teams && this.publications) {
                    this.flexIndividuals = 30;
                } else if (this.teams || this.publications) {
                    this.flexIndividuals = 45;
                } else {
                    this.flexIndividuals = 100;
                }
            }else{
                this.publications = false;
                this.teams = false;
                this.flexIndividuals = 100;
            }
        }
        if (this.publications) {
            if(!this.mobileCheck() && window.innerWidth >= 1600) {
                if (this.individuals && this.teams) {
                    this.flexPublications = 40;
                } else if (this.individuals || this.teams) {
                    this.flexPublications = 45;
                } else {
                    this.flexPublications = 100;
                }
            }else{
                this.individuals = false;
                this.teams = false;
                this.flexPublications = 100;
            }
        }
      }

      onChangeIndiv(){
        if(this.individuals){
            if(!this.mobileCheck() && window.innerWidth >= 1600) {
                if (this.teams && this.publications) {
                    this.flexIndividuals = 30;
                } else if (this.teams || this.publications) {
                    this.flexIndividuals = 45;
                } else {
                    this.flexIndividuals = 100;
                }
            }else{
                this.publications = false;
                this.teams = false;
                this.flexIndividuals = 100;
            }
        }
        if(this.teams){
            if(!this.mobileCheck() && window.innerWidth >= 1600) {
                if (this.individuals && this.publications) {
                    this.flexTeams = 30;
                } else if (this.individuals || this.publications) {
                    this.flexTeams = 45;
                } else {
                    this.flexTeams = 100;
                }
            }else{
                this.publications = false;
                this.individuals = false;
                this.flexTeams = 100;
            }
        }
        if (this.publications) {
            if(!this.mobileCheck() && window.innerWidth >= 1600) {
                if (this.individuals && this.teams) {
                    this.flexPublications = 40;
                } else if (this.individuals || this.teams) {
                    this.flexPublications = 45;
                } else {
                    this.flexPublications = 100;
                }
            }else{
                this.individuals = false;
                this.teams = false;
                this.flexPublications = 100;
            }
        }
      }

      onChangePub(){
        if (this.publications) {
            if(!this.mobileCheck() && window.innerWidth >= 1600) {
                if (this.individuals && this.teams) {
                    this.flexPublications = 40;
                } else if (this.individuals || this.teams) {
                    this.flexPublications = 45;
                } else {
                    this.flexPublications = 100;
                }
            }else{
                this.individuals = false;
                this.teams = false;
                this.flexPublications = 100;
            }
        }
        if(this.individuals){
            if(!this.mobileCheck() && window.innerWidth >= 1600) {
                if (this.teams && this.publications) {
                    this.flexIndividuals = 30;
                } else if (this.teams || this.publications) {
                    this.flexIndividuals = 45;
                } else {
                    this.flexIndividuals = 100;
                }
            }else{
                this.publications = false;
                this.teams = false;
                this.flexIndividuals = 100;
            }
        }
        if(this.teams){
            if(!this.mobileCheck() && window.innerWidth >= 1600) {
                if (this.individuals && this.publications) {
                    this.flexTeams = 30;
                } else if (this.individuals || this.publications) {
                    this.flexTeams = 45;
                } else {
                    this.flexTeams = 100;
                }
            }else{
                this.publications = false;
                this.individuals = false;
                this.flexTeams = 100;
            }
        }
    }

    /**
    * Fire when user tap on keyboard
    * @param keyCode : key of keyboard user
    */
    // eventHandler(keyCode){
    //     if(keyCode == 13){
    //         this.textSearch(this.searchText)
    //     }
    // }

    /**
    *    Go to Top screen
    */
    goToTop(){
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    /**
    * Use to check if is a mobile or not to display list instead of cards
    * @returns {boolean} : true if is a mobile
    */
    mobileCheck() {
        let check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor);
        return check;
    };

}
