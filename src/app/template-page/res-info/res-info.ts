import {Component, Input, Output, EventEmitter, OnChanges, SimpleChange} from '@angular/core';

 @Component({
     selector : 'res-info',
     templateUrl : './res-info.html'
 })

 /**
  * Use to display the info of the result number of affiliations find, current page
  * Only Element
  */
export class ResInfo{

    @Input() resultStat;
    @Input() currentPage;
    @Input() pageSize;
    @Output() onChangeSizePage = new EventEmitter();
    pageSelectSize;

    //choose number element to display
    pageSizes = [
        {count: 5, label: "5 per page"},
        {count: 10, label: "10 per page"},
        {count: 20, label: "20 per page"},
        {count: 50, label: "50 per page"}
    ];

    constructor(){}

    /**
     * Select page size in md-select
     * @param pageSize : the number of result to display
     */
    selectPageSize(pageSelectSize){
        this.onChangeSizePage.emit({pageSize : pageSelectSize});
    }


        ngOnChanges(changes: {[propKey: string]: SimpleChange}){
            for (let propName in changes) {
            if(propName == 'resultStat'){
                    this.resultStat = changes['resultStat'].currentValue;
                }
              }
            }

}
