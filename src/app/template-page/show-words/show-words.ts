import {Component, Input} from '@angular/core';

 @Component({
     selector : 'show-words',
     templateUrl : './show-words.html',
     styleUrls : ['./show-words.css']
 })

export class ShowWords{

    //Word to display
    @Input() words;
    morewords = false;
    nbwords = 15;

    constructor(){}

   /**
    * Use to display word (less or more)
    * @param val : boolean set to true if you need more word else set to false
    */
   moreWords(val) {
       this.morewords = val;
       if(val){
           this.nbwords = this.words.length;
       }else{
           this.nbwords = 15;
       }
   }

}
