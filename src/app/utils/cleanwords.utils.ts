import  * as levenshtein from 'fast-levenshtein';

export class CleanWords{

    constructor() {}

    /**
     * Clean keywords with dist of levenshtein and get bigrams or trigrams
     * @param words : list of words get from elasticsearch
     * @returns {{words: *, bigrams: Array}} : list of new words and list of bigrams and trigrams
     */
    getCleanKeywords(words) {
        for (let k = 0; k < words.length; k++) {
            let dist = 10;
            let min_dist = 10;
            for (let i = 0; i < words.length; i++) {
                if (k != i) {
                    dist = levenshtein.get(words[k].text, words[i].text);
                    if (min_dist > dist) {
                        min_dist = dist;
                    }
                }
            }
            if (min_dist <= 2) {
                words.splice(k, 1);
                k = 0;
            }
        }
        let bigramExplain = [];
        for (let i = 0; i < words.length; i++) {
            let isbigram = words[i].text.split(" ");
            if (isbigram[1] != undefined) {
                bigramExplain.push(words[i].text);
                words.splice(i, 1);
                i = 0;
            }
        }
      return {words : words, bigrams : bigramExplain};
    }

    /**
     * Remove words ith distance of levenshtein <= 2
     * @param words : list of words
     * @returns {*} : clean list of words
     */
    getCleanLevenshtein(words){
        for(let k=0; k < words.length; k++) {
            let dist = 10;
            let min_dist = 10;
            let posOfMinDist = 0;
            for(let i=0; i < words.length; i++) {
                if(k != i){
                    dist = levenshtein.get(words[k].text, words[i].text);
                    if (min_dist > dist) {
                        min_dist = dist;
                        posOfMinDist = i;
                    }
                }
            }
            if(min_dist <= 2){
                //calculate score new score
                words[posOfMinDist].size = words[posOfMinDist].size + words[k].size;
                words.splice(k, 1);
                k=0;
            }
        }
        return words;
    }

    /**
     * Remove words ith distance of levenshtein <= 2
     * @param words : list of words
     * @returns {*} : clean list of words
     */
    getCleanLevenshteinBigrams(words){
        for(let k=0; k < words.length; k++) {
            let dist = 10;
            let min_dist = 10;
            let posOfMinDist = 0;
            for(let i=0; i < words.length; i++) {
                if(k != i){
                    dist = levenshtein.get(words[k].text, words[i].text);
                    if (min_dist > dist) {
                        min_dist = dist;
                        posOfMinDist = i;
                    }
                }
            }
            if(min_dist <= 2){
                //calculate score new score
                if(words[posOfMinDist].text.split(" ").length > 1){
                    words.splice(k, 1);
                    k=0;
                }else{
                    words[posOfMinDist].size = words[posOfMinDist].size + words[k].size;
                    words.splice(k, 1);
                    k=0;
                }
            }
        }
        return words;
    }

    /**
     * Get bigrams from the elasticsaerch search
     * @param words : list of words get from elasticsearch search
     * @returns {{words: *, bigrams: Array}} : list of new words and list of bigrams and trigrams
     */
    getBigrams(words){
        let bigramExplain = [];
        for(let i = 0; i < words.length; i++){
            let isbigram = words[i].text.split(" ");
            if(isbigram[1] != undefined){
                bigramExplain.push(words[i].text);
                words.splice(i, 1);
                i = i - 1;
            }
        }
        return {words : words, bigrams : bigramExplain};
    }

    /**
     * Get significant keywords from a team or a user
     * @param result_fr : list of significant keywords
     * @returns {Array} : list of clean significant keywords
     */
    getSignificantKeywords(result_fr, affiliationId){
        for(let k=0; k < result_fr.length; k++) {
            let dist = 10;
            let min_dist = 10;
            for(let i=0; i < result_fr.length; i++) {
                if(k != i){
                    dist = levenshtein.get(result_fr[k].key, result_fr[i].key);
                    if (min_dist > dist) {
                        min_dist = dist;
                    }
                }
            }
            if(min_dist <= 2){
                result_fr.splice(k, 1);
                k=0;
            }
        }
        let wordsallkeycloud = [];
        for(let i = 0; i < result_fr.length; i++){
            if(result_fr[i].score > 10){
                if(wordsallkeycloud.indexOf(result_fr[i].key) == -1){
                    if(wordsallkeycloud.length < 30){
                        let replaceApos = result_fr[i].key.replace("'", ' ');
                        wordsallkeycloud.push({text : replaceApos, size : result_fr[i].score, affiliationId : affiliationId, userId : affiliationId});
                    }
                }
            }
        }
        return wordsallkeycloud;
    }

  }
