export class UserUtils{

    constructor(){}
    // Explanation logic use when click on the button of a user like explanation
    // USE FOR USER SEARCH
    /**
     * Use to explain why this user is choose with the given competencies (word more or less important)
     * @param userId : id of the user
     * @param searchResp : response give by elasticsearch
     * @returns {Array} : word of the explaination
     */
    userSearchExplain(userId, searchResp) {
        // Get all the respond from the search
        let jsonUpperHits = searchResp;
        for (let i = 0; i < jsonUpperHits.length; i++) {
            let upperHits = jsonUpperHits[i];
            let user_id = upperHits._id;
            if (user_id == userId) {
                // Get publications of the user
                let jsonInnerHits = upperHits.inner_hits["pubs"].hits.hits;
                let explainTermsLevelArray = [];
                for (let j = 0; j < jsonInnerHits.length; j++) {
                    let innerHits = jsonInnerHits[j];
                    // Get explanation about the publication
                    let explanationObjects = innerHits._explanation;
                    for(let k=0; k < explanationObjects.details[0].details[0].details[0].details.length;  k++) {
                        let temp_explanationObject = explanationObjects.details[0].details[0].details[0].details[k];
                        if(temp_explanationObject.description.indexOf(("weight")) == -1) {
                            let old_term = temp_explanationObject;
                            while (temp_explanationObject.description.indexOf(("weight")) == -1) {
                                old_term = temp_explanationObject;
                                temp_explanationObject = temp_explanationObject.details[0];
                            }
                            for (let w = 0; w < old_term.details.length; w++) {
                                let explanationDetails = old_term.details[w];
                                let description = explanationDetails.description;
                                description = description.substring(description.indexOf(":") + 1, description.lastIndexOf("in")).trim();
                                let termScore = explanationDetails.value;
                                if (description != "product of:" && description.indexOf("product of:") == -1 && description != "sum of:" && description.indexOf("sum of:") == -1) {
                                    explainTermsLevelArray.push({
                                        "text": description,
                                        "size": termScore,
                                        "affiliationId" : userId,
                                        "userId" : userId
                                    });
                                }
                            }
                        }else {
                            let old_term = temp_explanationObject;
                            let description = old_term.description;
                            description = description.substring(description.indexOf(":") + 1, description.lastIndexOf("in")).trim();
                            let termScore = old_term.value;
                            if (description != "product of:" && description.indexOf("product of:") == -1 && description != "sum of:" && description.indexOf("sum of:") == -1) {
                                explainTermsLevelArray.push({
                                    "text": description,
                                    "size": termScore,
                                    "affiliationId" : userId,
                                    "userId" : userId
                                });
                            }
                        }
                    }
                }
                return explainTermsLevelArray;
            }
        }
    }

    userSearchExplainBigrams(userId, searchResp) {
        // Get all the respond from the search
        let jsonUpperHits = searchResp;
        for (let i = 0; i < jsonUpperHits.length; i++) {
            let upperHits = jsonUpperHits[i];
            let user_id = upperHits._id;
            if (user_id == userId) {
                // Get publications of the user
                let jsonInnerHits = upperHits.inner_hits["pubs"].hits.hits;
                let explainTermsLevelArray = [];
                for (let j = 0; j < jsonInnerHits.length; j++) {
                    let innerHits = jsonInnerHits[j];
                    // Get explanation about the publication
                    let explanationObjects = innerHits._explanation;
                    for(let k=0; k < explanationObjects.details[0].details.length;  k++) {
                        let temp_explanationObject = explanationObjects.details[k];
                        if(temp_explanationObject.description.indexOf(("weight")) == -1) {
                            let old_term = temp_explanationObject;
                            while (temp_explanationObject.description.indexOf(("weight")) == -1) {
                                old_term = temp_explanationObject;
                                temp_explanationObject = temp_explanationObject.details[0];
                            }
                            for (let w = 0; w < old_term.details.length; w++) {
                                let explanationDetails = old_term.details[w];
                                let description = explanationDetails.description;
                                description = description.substring(description.indexOf(":") + 1, description.lastIndexOf("in")).trim();
                                let termScore = explanationDetails.value;
                                if (description != "product of:" && description.indexOf("product of:") == -1 && description != "sum of:" && description.indexOf("sum of:") == -1) {
                                    explainTermsLevelArray.push({
                                        "text": description,
                                        "size": termScore,
                                        "affiliationId" : userId,
                                        "userId" : userId
                                    });
                                }
                            }
                        }else {
                            let old_term = temp_explanationObject;
                            let description = old_term.description;
                            description = description.substring(description.indexOf(":") + 1, description.lastIndexOf("in")).trim();
                            let termScore = old_term.value;
                            if (description != "product of:" && description.indexOf("product of:") == -1 && description != "sum of:" && description.indexOf("sum of:") == -1) {
                                explainTermsLevelArray.push({
                                    "text": description,
                                    "size": termScore,
                                    "affiliationId" : userId,
                                    "userId" : userId
                                });
                            }
                        }
                    }
                }
                return explainTermsLevelArray;
            }
        }
    }

    affserSearchExplainBigrams(userId, searchResp) {
        // Get all the respond from the search
        let jsonUpperHits = searchResp;
        for (let i = 0; i < jsonUpperHits.length; i++) {
            let upperHits = jsonUpperHits[i];
            let user_id = upperHits._id;
            if (user_id == userId) {
                // Get publications of the user
                let jsonInnerHits = upperHits.inner_hits["pubs"].hits.hits;
                let explainTermsLevelArray = [];
                for (let j = 0; j < jsonInnerHits.length; j++) {
                    let innerHits = jsonInnerHits[j];
                    // Get explanation about the publication
                    let explanationObjects = innerHits._explanation;
                    for(let k=0; k < explanationObjects.details[0].details.length;  k++) {
                        let temp_explanationObject = explanationObjects.details[k];
                        if(temp_explanationObject.description.indexOf(("weight")) == -1) {
                            let old_term = temp_explanationObject;
                            while (temp_explanationObject.description.indexOf(("weight")) == -1) {
                                old_term = temp_explanationObject;
                                temp_explanationObject = temp_explanationObject.details[0];
                            }
                            for (let w = 0; w < old_term.details.length; w++) {
                                let explanationDetails = old_term.details[w];
                                let description = explanationDetails.description;
                                description = description.substring(description.indexOf(":") + 1, description.lastIndexOf("in")).trim();
                                let termScore = explanationDetails.value;
                                if (description != "product of:" && description.indexOf("product of:") == -1 && description != "sum of:" && description.indexOf("sum of:") == -1) {
                                    explainTermsLevelArray.push({
                                        "text": description,
                                        "size": termScore,
                                        "affiliationId" : userId,
                                        "userId" : userId
                                    });
                                }
                            }
                        }else {
                            let old_term = temp_explanationObject;
                            let description = old_term.description;
                            description = description.substring(description.indexOf(":") + 1, description.lastIndexOf("in")).trim();
                            let termScore = old_term.value;
                            if (description != "product of:" && description.indexOf("product of:") == -1 && description != "sum of:" && description.indexOf("sum of:") == -1) {
                                explainTermsLevelArray.push({
                                    "text": description,
                                    "size": termScore,
                                    "affiliationId" : userId,
                                    "userId" : userId
                                });
                            }
                        }
                    }
                }
                return explainTermsLevelArray;
            }
        }
    }
    /**
     * Use to explain why this user is choose with the given competencies (word more or less important)
     * @param userId : id of the user
     * @param searchResp : response give by elasticsearch
     * @returns {Array} : word of the explaination
     */
    affSearchExplain(userId, searchResp) {
        // Get all the respond from the search
        let jsonUpperHits = searchResp.hits.hits;
        for (let i = 0; i < jsonUpperHits.length; i++) {
            let upperHits = jsonUpperHits[i];
            let user_id = upperHits._id;
            if (user_id == userId) {
                //let explainDocsLevelArray = [];
                // Get publications of the user
                let jsonInnerHits = upperHits.inner_hits["pubs"].hits.hits;
                let explainTermsLevelArray = [];
                for (let j = 0; j < jsonInnerHits.length; j++) {
                    let innerHits = jsonInnerHits[j];
                    // Get explanation about the publication
                    let explanationObjects = innerHits._explanation;
                    for(let k=0; k < explanationObjects.details.length;  k++) {
                        let temp_explanationObject = explanationObjects.details[k];
                        if(temp_explanationObject.description.indexOf(("weight")) == -1){
                            let old_term = temp_explanationObject;
                            while (temp_explanationObject.description.indexOf(("weight")) == -1) {
                                old_term = temp_explanationObject;
                                temp_explanationObject = temp_explanationObject.details[0];
                            }
                                for(let w=0; w <old_term.details.length; w++){
                                    let explanationDetails = old_term.details[w];
                                    let description = explanationDetails.description;
                                    description = description.substring(description.indexOf(":") + 1, description.lastIndexOf("in")).trim();
                                    let termScore = explanationDetails.value;
                                    if (description != "product of:" && description.indexOf("product of:") == -1 && description != "sum of:" && description.indexOf("sum of:") == -1) {
                                        explainTermsLevelArray.push({
                                            "text": description,
                                            "size": termScore,
                                            "affiliationId" : userId,
                                            "userId" : userId
                                        });
                                    }
                                }
                            }
                        else{
                            let old_term = temp_explanationObject;
                            let description = old_term.description;
                            description = description.substring(description.indexOf(":") + 1, description.lastIndexOf("in")).trim();
                            let termScore = old_term.value;
                            if (description != "product of:" && description.indexOf("product of:") == -1 && description != "sum of:" && description.indexOf("sum of:") == -1) {
                                explainTermsLevelArray.push({
                                    "text": description,
                                    "size": termScore,
                                    "affiliationId" : userId,
                                    "userId" : userId
                                });
                            }
                        }

                    }
                }
                return explainTermsLevelArray;
            }
        }
    }

    /**
     * Use to explain why this user is choose with the given competencies (word more or less important)
     * @param userId : id of the user
     * @param searchResp : response give by elasticsearch
     * @returns {Array} : word of the explaination
     */
    affSearchExplainBigram(teamId, searchResp) {
        // Get all the respond from the search
        let jsonUpperHits = searchResp;
        //let explainDocsLevelArray = [];
        // Get publications of the user
        let jsonInnerHits = searchResp.hits.hits[0].inner_hits["pubs"].hits.hits;
        let explainTermsLevelArray = [];
        for (let j = 0; j < jsonInnerHits.length; j++) {
            let innerHits = jsonInnerHits[j];
            // Get explanation about the publication
            let explanationObjects = innerHits._explanation;
            for(let k=0; k < explanationObjects.details.length;  k++) {
                let temp_explanationObject = explanationObjects.details[k];
                if(temp_explanationObject.description.indexOf(("weight")) == -1){
                    let old_term = temp_explanationObject;
                    while (temp_explanationObject.description.indexOf(("weight")) == -1) {
                        old_term = temp_explanationObject;
                        temp_explanationObject = temp_explanationObject.details[0];
                    }
                        for(let w=0; w <old_term.details.length; w++){
                            let explanationDetails = old_term.details[w];
                            let description = explanationDetails.description;
                            description = description.substring(description.indexOf(":") + 1, description.lastIndexOf("in")).trim();
                            let termScore = explanationDetails.value;
                            if (description != "product of:" && description.indexOf("product of:") == -1 && description != "sum of:" && description.indexOf("sum of:") == -1) {
                                explainTermsLevelArray.push({
                                    "text": description,
                                    "size": termScore,
                                    "affiliationId" : teamId,
                                    "userId" : teamId
                                });
                            }
                        }
                    }
                else{
                    let old_term = temp_explanationObject;
                    let description = old_term.description;
                    description = description.substring(description.indexOf(":") + 1, description.lastIndexOf("in")).trim();
                    let termScore = old_term.value;
                    if (description != "product of:" && description.indexOf("product of:") == -1 && description != "sum of:" && description.indexOf("sum of:") == -1) {
                        explainTermsLevelArray.push({
                            "text": description,
                            "size": termScore,
                            "affiliationId" : teamId,
                            "userId" : teamId
                        });
                    }
                }

            }
        }
        return explainTermsLevelArray;
    }

    /**
     * Use to explain why this user is choose with the given competencies (word more or less important)
     * @param userId : id of the user
     * @param searchResp : response give by elasticsearch
     * @returns {Array} : word of the explaination
     */
    userSearchExplainTeamKeywords(userId, searchResp) {
        // Get all the respond from the search
        let jsonUpperHits = searchResp;
        for (let i = 0; i < jsonUpperHits.length; i++) {
            let upperHits = jsonUpperHits[i];
            let user_id = upperHits._id;
            if (user_id == userId) {
                // Get publications of the user
                let jsonInnerHits = upperHits.inner_hits["pubs"].hits.hits;
                let explainTermsLevelArray = [];
                for (let j = 0; j < jsonInnerHits.length; j++) {
                    let innerHits = jsonInnerHits[j];
                    // Get explanation about the publication
                    let explanationObjects = innerHits._explanation;
                    for(let k=0; k < explanationObjects.details[0].details[0].details.length;  k++) {
                        let temp_explanationObject = explanationObjects.details[0].details[0].details[k];
                        if(temp_explanationObject.description.indexOf(("weight")) == -1) {
                            let  old_term = temp_explanationObject;
                            while (temp_explanationObject.description.indexOf(("weight")) == -1) {
                                old_term = temp_explanationObject;
                                temp_explanationObject = temp_explanationObject.details[0];
                            }
                            for (let w = 0; w < old_term.details.length; w++) {
                                let explanationDetails = old_term.details[w];
                                let description = explanationDetails.description;
                                description = description.substring(description.indexOf(":") + 1, description.lastIndexOf("in")).trim();
                                let termScore = explanationDetails.value;
                                if (description != "product of:" && description.indexOf("product of:") == -1 && description != "sum of:" && description.indexOf("sum of:") == -1) {
                                    explainTermsLevelArray.push({
                                        "text": description,
                                        "size": termScore,
                                        "affiliationId" : userId,
                                        "userId" : userId
                                    });
                                }
                            }
                        }else {
                            let old_term = temp_explanationObject;
                            let description = old_term.description;
                            description = description.substring(description.indexOf(":") + 1, description.lastIndexOf("in")).trim();
                            let termScore = old_term.value;
                            if (description != "product of:" && description.indexOf("product of:") == -1 && description != "sum of:" && description.indexOf("sum of:") == -1) {
                                explainTermsLevelArray.push({
                                    "text": description,
                                    "size": termScore,
                                    "affiliationId" : userId,
                                    "userId" : userId
                                });
                            }
                        }
                    }
                }
                return explainTermsLevelArray;
            }
        }
    }


    /**
     * Normalize score for most significant keywords
     * @param explainJson : all keywords and score
     * @returns {*} : all keywords and new score
     */
    normalizeScore(explainJson){
        let allwords = explainJson;
        allwords.sort(function(a, b){
            return b.size - a.size;
        });
        let max_norm = allwords[0].size;
        for(let i = 0; i < allwords.length; i++){
            allwords[i] = {
                "text": allwords[i].text,
                "size": ((0.2 - 1)*((allwords[i].size - allwords[allwords.length - 1].size) / (max_norm - allwords[allwords.length - 1].size)) + 1)* 70,
                "affiliationId" : allwords[i].affiliationId,
                "userId" : allwords[i].userId
            };
        }
        return allwords;
    }

    /**
     * Normalize score for most significant keywords
     * @param explainJson : all keywords and score
     * @returns {*} : all keywords and new score
     */
    normalizeScoreBigrams(explainJson){
        let allwords = explainJson;
        allwords.sort(function(a, b){
            return b.size - a.size;
        });
        let max_norm = allwords[0].size;
        for(let i = 0; i < allwords.length; i++){
            allwords[i] = {
                "text": allwords[i].text,
                "size": ((0.2 - 1)*((allwords[i].size - allwords[allwords.length - 1].size) / (max_norm - allwords[allwords.length - 1].size)) + 1)* 50,
                "affiliationId" : allwords[i].affiliationId,
                "userId" : allwords[i].userId
            };
        }
        return allwords;
    }

    /**
     * Normalize request explanation
     * @param explainJson : all words in explanation
     * @returns {*} : new score of explanation words
     */
    normalizeScoreRequest(explainJson){
        let allwords = explainJson;
        allwords.sort(function(a, b){
            return a.size - b.size;
        });
        let words_remove = [];
        for(let i = 0; i < allwords.length; i++) {
            if (allwords[i].text.split(" ").length > 1) {
                words_remove.push(allwords[i]);
                allwords.splice(i, 1);
                i = i - 1;
            }
        }
        let min_norm = allwords[0].size;
        for(let i = 0; i < allwords.length; i++){
                allwords[i] = {
                    "text": allwords[i].text,
                    "size": ((0.5 - 1)*((allwords[i].size - min_norm) / (allwords[allwords.length - 1].size - min_norm)) + 1) * 50,
                    "affiliationId" : allwords[i].affiliationId,
                    "userId" : allwords[i].userId
                };
        }
        allwords = allwords.concat(words_remove);
        return allwords;
    }

    //USE FOR USER RELATIONS
    /**
     * Find why we get this user relation to the other with the words in title and summary of publication
     * @param _userId : id of the user for the explanation
     * @returns {Array} : words find for explanation with score
     */
    similarUserSearchExplain(_userId, searchResp) {
        let copySearchResp = searchResp;
        let jsonUpperHits = copySearchResp.hits.hits;
        for (let i = 0; i < jsonUpperHits.length; i++) {
            let upperHits = jsonUpperHits[i];
            let user_id = upperHits._id;
            if (user_id == _userId) {
                //let explainDocsLevelArray = [];
                // get inner_hits for the score of the publication
                let jsonInnerHits = upperHits.inner_hits["pubs"].hits.hits;
                // for all the publication in List
                let explainTermsLevelArray = [];
                for (let j = 0; j < jsonInnerHits.length; j++) {
                    let innerHits = jsonInnerHits[j];
                    // get the explanation about the score of publication
                    let explanationObject = innerHits._explanation;
                    // get details of the inner_hits
                    let explanationDetails = explanationObject.details[0].details[0];
                    // value of the publication queryWeight
                    let coefficient = explanationObject.details[0].value;
                    for (let r = 0; r < explanationDetails.length; r++) {
                        //weight(doc.publicationList.title:probabilistic in 1437) [PerFieldSimilarity], result of
                        let description = explanationDetails[r].description;
                        //title or summary
                        let explainField = description.substring(description.indexOf("(") + 1, description.indexOf(":")).trim();
                        //words find in the field
                        let term = description.substring(description.indexOf(":") + 1, description.lastIndexOf("in")).trim();
                        //calcul du score
                        let term_score = explanationDetails[r].value * coefficient;
                        explainTermsLevelArray.push({
                            "text": explainField,
                            "text_2": term,
                            "size": term_score
                        });
                    }
                }
                return explainTermsLevelArray;
            }
        }
    }

    // Explanation logic seem to be not use anymore WAS IN MATCH SEARCH match schow only by Name now
    /**
     * Use to explain why this user is choose with the competencies input by the user
     * @param _userId : id f the user to explain
     * @returns {Array} : array of words to explain
     */
    userSearchExplainMatch(_userId, searchResp) {
        let copySearchResp =   searchResp;
        let totalHits = copySearchResp.hits.total.value;
        let explainDocsLevelArray = [];
        let jsonUpperHits = copySearchResp.hits.hits;
        for (let i = 0; i < jsonUpperHits.length; i++) {
            let upperHits = jsonUpperHits[i];
            let userId = upperHits._id;
            if (userId == _userId) {
                let jsonInnerHits = upperHits.inner_hits.publication.hits.hits;
                let explainTermsLevelArray = [];
                for (let j = 0; j < jsonInnerHits.length; j++) {
                    let innerHits = jsonInnerHits[j];
                    let publication_id = innerHits._id;
                    let explanationObject = innerHits._explanation;
                    if (explanationObject.description.indexOf(("sum of:")) != -1) {
                        let explanation = explanationObject.details;
                        for (let k = 0; k < explanation.length; k++) {
                            let termScore = explanation[k].value;
                            let description = explanation[k].description;
                            let explainField = description.substring(description.indexOf("(") + 1, description.indexOf(":")).trim();
                            let term = description.substring(description.indexOf(":") + 1, description.lastIndexOf("in")).trim();
                            explainTermsLevelArray.push({
                                "field": explainField,
                                "term": term,
                                "termScore": termScore
                            });
                        }
                    } else if (explanationObject.description.indexOf("product of:") != -1) {
                        let explanationDetails = explanationObject.details[0].details;
                        let coefficient = explanationObject.details[1].value;
                        for (let r  in explanationDetails) {
                            let termScore = explanationDetails[r].value * coefficient;
                            let description = explanationDetails[r].description;
                            let explainField = description.substring(description.indexOf("(") + 1, description.indexOf(":")).trim();
                            let term = description.substring(description.indexOf(":") + 1, description.lastIndexOf("in")).trim();
                            explainTermsLevelArray.push({
                                "field": explainField,
                                "term": term,
                                "termScore": termScore
                            });
                        }
                    } else {
                        let termScore = explanationObject.value;
                        let description = explanationObject.description;
                        let explainField = description.substring(description.indexOf("(") + 1, description.indexOf(":")).trim();
                        let term = description.substring(description.indexOf(":") + 1, description.lastIndexOf("in")).trim();
                        explainTermsLevelArray.push({
                            "field": explainField,
                            "term": term,
                            "termScore": termScore
                        });
                    }
                }
                return explainTermsLevelArray;
            }
        }
    }

}
