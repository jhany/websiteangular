import {Component, Input, OnChanges, SimpleChange} from '@angular/core';
import {MatDialog} from '@angular/material';
import {SearchIndivService} from '../../services/es-indiv-serv';
import {UserUtils} from '../../utils/user.utils';
import {CleanWords} from '../../utils/cleanwords.utils';
import {DialogWordsCloud} from '../../template-page/dialog-words-cloud/dialog-words-cloud';
import {LocalStorageService} from '../../services/localStorageService';
import {Globals} from '../../globals'
import { MatomoTracker } from 'ngx-matomo';
import { Location } from '@angular/common';

/*
*    Component to display individuals in list
*/
@Component({
    selector : 'list-individuals',
    templateUrl : './list-results.html'
})

export class ListResults {

    @Input() users;
    @Input() isSearchUser;
    @Input() isTeamKeywords;
    @Input() isBigram;
    @Input() explanationBtn;
    @Input() textSearch;

    //map number of publication and user
    private mapNbPub = new Map();
    private globals : Globals = new Globals();
    userUtils = new UserUtils();
    cleanWords = new CleanWords();
    current_year = 0;

    constructor(private dialog : MatDialog, private localStorageService: LocalStorageService, private searchIndivService : SearchIndivService, private matomoTracker: MatomoTracker, private location : Location){
        this.current_year = new Date().getFullYear() - 1;
    }

    /**
    * Remove words not useful for the explanation
    * @param userId : id of the user
    * @returns {*} : list of the new words
    */
    private formattedSimilarUserSearchExplain(userId) {
        let url_path = this.location.path().split('/');
        let idUser = url_path[url_path.length - 1]
        this.searchIndivService.getSimilarUsersExplain(idUser, userId).then(
            resp => {
            let explainJson = this.userUtils.affSearchExplain(userId, resp);
            let resultwordtext = this.cleanWords.getCleanLevenshtein(explainJson);
            resultwordtext = this.userUtils.normalizeScore(resultwordtext);
            let msg = "Similar keywords";
            let words = this.userUtils.normalizeScore(resultwordtext);
            this.openDialog(words, msg, []);
        }
    )

    }
    /**
    * Open a md Dialog with word cloud
    * @param bigrams : bigrams to show
    * @param msg : message to show one to of the dialog
    * @param items : words to show in the dialog
    */
    private openDialog(items, msg,bigrams){
        let dial = this.dialog.open(DialogWordsCloud, {
            data : {msg : "Explanation of the return : "+msg, words : items, bigrams : bigrams}
        });
    };

    /**
    * Go the explanation about the user and show the dialog
    * @param userId : id of the user
    * @param firstName : first name of the user
    * @param lastName : last name of the user
    */
    showCloud(userId, firstName, lastName) {
        if(!this.isSearchUser){
            this.formattedSimilarUserSearchExplain(userId);
        }else if(this.isBigram) {
            let text = this.textSearch;
            if (text.indexOf('"') != -1 && text.indexOf('"') != text.lastIndexOf('"')) {
                // text = text.replace('"', '');
                // text = text.replace('"', '');
                let words = this.userUtils.userSearchExplainBigrams(userId, this.users.hits.hits);
                let cleanWords = this.userUtils.normalizeScoreBigrams(words);
                cleanWords = this.cleanWords.getCleanLevenshteinBigrams(cleanWords);

                //let messageSplit = this.textSearch.split(" ");
                let msg = this.textSearch;
                this.openDialog(cleanWords, msg, []);
            } else {
                    let words = this.userUtils.userSearchExplain(userId, this.users.hits.hits);
                    let cleanWords = this.cleanWords.getCleanLevenshtein(words);
                    cleanWords = this.userUtils.normalizeScoreRequest(cleanWords);
                    //let messageSplit = this.textSearch.split(" ");
                    let msg = this.textSearch;
                    this.openDialog(cleanWords, msg, []);
                }
        }else if(this.isTeamKeywords){
            let words = this.userUtils.userSearchExplainTeamKeywords(userId, this.users.hits.hits);
            let cleanWords = this.userUtils.normalizeScoreRequest(words);
            cleanWords = this.cleanWords.getCleanLevenshtein(cleanWords);

            //let messageSplit = this.textSearch.split(" ");
            let msg = this.textSearch;
            this.openDialog(cleanWords, msg, []);
        }
        else{
            let words = this.formattedUserSearchExplain(userId, this.users.hits.hits);
            let cleanWords = this.cleanWords.getCleanKeywords(words);
            //let messageSplit = this.textSearch.split(" ");
            let msg = this.textSearch;
            this.openDialog(cleanWords.words, msg, cleanWords.bigrams);
        }
    };


    /**
    * saveUser : Save a user in the basket
    * @param userId : Id of the user
    * @param firstName : first name of the user
    * @param lastName : last name of the user
    */
    saveUser(userId, firstName, lastName) {
        let data = localStorage.getItem("data");
        let list_save = [];
        if (data == undefined || data == "undefined") {
            list_save = [];
        } else {
            list_save = JSON.parse(data);
        }
        for (let i in list_save) {
            if (list_save[i].id == userId) {
                return;
            }
        }
        if(this.isSearchUser){
            list_save.push({
                "id": userId,
                "name": firstName + " " + lastName,
                "link": this.globals.url+"/individual/" + userId,
                "type": "individual",
                "search" : this.textSearch
            });
        }else{
            list_save.push({
                "id": userId,
                "name": firstName + " " + lastName,
                "link": this.globals.url+"/individual/" + userId,
                "type": "individual",
                "search" : ""
            });
        }
        this.matomoTracker.trackEvent('Indiv_save', userId, this.textSearch);
        this.localStorageService.set(list_save);
    }


    /**
    * Encode the search of the user when it change
    * @param optionChange : get the new search if the user
    */
    ngOnChanges(changes: {[propKey: string] : SimpleChange}) {
        for (let propName in changes) {
        if (propName == 'users') {
                this.users = changes[propName].currentValue;
            }
        }
    }

    /**
    * Use the word to format them for display the in the word cloud with different size (importance)
    * @param userId : user id
    * @returns {Array} : list of words format
    */
    private formattedUserSearchExplain(userId, result) {
        let explainJson = this.userUtils.userSearchExplain(userId, result);
        return this.userUtils.normalizeScoreRequest(explainJson);
    }

    /**
    * Get and show the dialog the main topic of the user
    * @param userId : id of the user
    * @param firstName : first name of the user
    * @param lastName : last name of the uer
    */
    showResearcherMainTopics(userId, firstName, lastName, pubs) {
        let msg = firstName + " " + lastName + " main keywords";
        let all_halId = [];
        for(let i = 0; i < pubs.length; i++) {
            all_halId.push(pubs[i].halId);
        }
        this.searchIndivService.getSignificantKeywordsAll(all_halId).then(
             resp => {
                let result = resp.aggregations.all_pub.pub_filter.most_sig_words.buckets;
                let keywords = this.cleanWords.getSignificantKeywords(result, userId);
                this.openDialog(keywords, msg, []);
            }
        );
    };

    /**
    * Go in new tab with publication relate to keyword and user
    * @param id : id of the user
    * @param keywords : keywords enter in the search
    */
    goToPublicationBykeywords(id, keywords){
        window.open("/publications-individual-keyword/"+id+"/"+keywords);
    }

    /**
    * Go to similar individuals
    * @param id : id of the user
    */
    individualsRelations(id){
        window.open("/individuals-relations/"+id)
    }

    /**
    * Go to similar individuals
    * @param id : id of the user
    */
    individualsSimilars(id){
        window.open("/individuals-similars/"+id)
    }
    /**
    * Same clixk event with piwikEvent
    * @param UserId : id of the user
    * @param textSearch : text search in the start
    */
    piwikEvent(UserId, textSearch){
        this.matomoTracker.trackEvent('User_click', UserId, textSearch);
    }

    /**
    * Use to check if is a mobile or not to display list instead of cards
    * @returns {boolean} : true if is a mobile
    */
    mobileCheck() {
        let check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor);
        return check;
    };

}
