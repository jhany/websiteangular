// import { NgModule }  from '@angular/core';
// // import { RouterModule } from '@angular/router';
// import { FormsModule } from '@angular/forms';
// import { BrowserModule} from '@angular/platform-browser';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { CommonModule, LocationStrategy, HashLocationStrategy } from '@angular/common';
// import { MaterialModule } from '@angular/material';
// import { FlexLayoutModule } from "@angular/flex-layout";
// import 'hammerjs';
// // import { Angular2FontawesomeModule } from 'angular2-fontawesome';
// // import { HttpModule } from '@angular/http';
// import { AppRoutingModule } from './app-routing.module';
// import { AppComponent }         from './app.component';
//
// import 'hammerjs';
// import 'elasticsearch-browser';
// import 'fast-levenshtein';
// import 'fast-levenshtein/levenshtein';
// import { Footer } from './template-page/footer/footer';
// import { ToolBarTop } from './template-page/toolbar-top/toolbar-top';
//
// import { AllSearchModule } from './template-page/All-search/all_search.module';
//
//
// @NgModule({
//   imports: [ BrowserModule, AppRoutingModule, FormsModule, AllSearchModule, MaterialModule, FlexLayoutModule, BrowserAnimationsModule],
//   declarations: [ AppComponent, Footer, ToolBarTop],
//   providers : [ {provide: LocationStrategy, useClass: HashLocationStrategy} ],
//   exports : [Footer, ToolBarTop],
//   bootstrap: [ AppComponent ]
// })
//
// export class AppModule {}

import { NgModule }  from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { BrowserModule} from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { HttpClientModule }    from '@angular/common/http';
import { MatProgressSpinnerModule, MatIconModule, MatInputModule, MatSelectModule, MatCheckboxModule, MatListModule, MatTooltipModule, MatPaginatorModule, MatTabsModule, MatTableModule, MatChipsModule, MatDialogModule, MatToolbarModule, MatAutocompleteModule} from '@angular/material';
import { FlexLayoutModule } from "@angular/flex-layout";
import { Angular2FontawesomeModule } from 'angular2-fontawesome';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent }         from './app.component';
import 'hammerjs';
import 'd3-cloud';
import 'd3-dispatch';
//search page
import { AllSearch } from './template-page/All-search/all_search';

//teams
import { AffiliationsSimilar } from './affiliations/affiliation-similar/affiliationsSimilar';
import { AffiliationUsers} from './affiliations/affiliation-users/affiliation-users';
import { Affiliation } from './affiliations/affiliation-display/affiliation';
import { AffiliationDomain } from './affiliations/affiliation-domain/affiliation-domain';
import { AffiliationInfo } from './affiliations/affiliation-general/affiliation-info';
import {AffiliationsSearch} from './affiliations/affiliations-search/affiliations-search';
import  {ListResultsAff} from './affiliations/list-results-aff/list-result-lab';
import {AffiliationClustering} from './affiliations/affiliation-domain/affiliations-clustering';

//users
import { UsersSearch } from './users/users-search/users-search';
import { ListResults } from './users/list-results/list-individuals';
import { Profile } from './profile/profile-display/profile';
import { ProfileOverview } from './profile/profile-overview/profile-overview';
import { ProfileClustering } from './profile/profile-clustering/profile-clustering';
import { UsersSimilar } from './users/users-similars/user-similar';
import { UsersRelations } from './users/users-relations/users-relations';
import { UsersAffKeywords } from './users/users-aff-keywords/users-aff-keywords';

//publication
import {PublicationsSearch} from './publications/publications-search/publications-search';
import {PublicationsSimilar} from './publications/publications-similars/publications-similar';
import {AffiliationKeywords} from './publications/publication-affiliation-keywords/publication-lab-keywords';
import {PublicationsUserKeyword} from './publications/publications-user-keyword/publications-user-keywords';


//template
import { Footer } from './template-page/footer/footer';
import { Pagination } from './template-page/pagination/pagination';
import { ToolBarTop } from './template-page/toolbar-top/toolbar-top';
import { ResInfo } from './template-page/res-info/res-info';
import { ShowWords } from './template-page/show-words/show-words';
import { ShowWordsClick } from './template-page/show-words-click/show-words-click';
import { PublicationAffDisplay, DialogAbstract } from './template-page/publication-display/publications-display';
import { MakeComment } from './template-page/make-comment/makeComment';
import { DialogWordsCloud } from './template-page/dialog-words-cloud/dialog-words-cloud';

//d3
import {BarChart} from './d3_template/bar-char/bar-chart';
import {WordCloudAff} from './d3_template/word-cloud-aff/word-cloud-aff';
import {WordCloudDir} from './d3_template/word-cloud/word-cloud';
import {DiagramSubDomain} from './d3_template/diagram-subdomain/diagram-subdomain';
import {SocialGraphLab} from './d3_template/social-graph/social-graph-lab';
import {SocialGraph} from './d3_template/social-graph/social-graph';

//views
import {Primer} from './views/primer';
import {Legal} from './views/legal';
import {Contact} from './views/contact';
import {Credit} from './views/credit';
import {Cookies} from './views/cookies';
import {PersonalData} from './views/personaldata';
import {Basket} from './basket/basket-results/basket';
import {SendResults} from './basket/send-results/send-result';

//providers
import {SearchIndivService} from './services/es-indiv-serv';
import {SearchPubService} from './services/es-pub-serv';
import {SearchLabService} from './services/es-lab-serv';
import { WindowRefService } from './services/windowRefService';
import {LocalStorageService} from './services/localStorageService';
import {ClusteringService} from './services/clustering_service';
import {CommentService} from './services/comment-serv';

import {SendMailServive} from './services/send_mail'

//Track event piwik
import { MatomoModule } from 'ngx-matomo';


@NgModule({
  imports: [ BrowserModule, MatInputModule, BrowserAnimationsModule, MatSelectModule, MatTabsModule, MatProgressSpinnerModule, MatIconModule, MatCheckboxModule, MatListModule, MatPaginatorModule, AppRoutingModule, CommonModule, MatomoModule,
      MatTableModule, MatChipsModule, MatDialogModule, MatToolbarModule, MatTooltipModule, FormsModule, FlexLayoutModule, Angular2FontawesomeModule,  HttpClientModule, MatAutocompleteModule],
  declarations: [ AppComponent, AllSearch, ToolBarTop, Footer, Pagination, UsersSearch, ResInfo, ListResults, Primer, Legal, Contact, Basket, AffiliationsSearch, PersonalData, Credit,
                PublicationsSearch, ListResultsAff, PublicationAffDisplay, Profile, ProfileOverview, ProfileClustering, ShowWords, ShowWordsClick, BarChart, WordCloudAff, AffiliationClustering,
                MakeComment, AffiliationsSimilar, Affiliation, WordCloudDir, AffiliationUsers, DiagramSubDomain, SocialGraph, SocialGraphLab,
            PublicationsSimilar, AffiliationInfo, AffiliationDomain, AffiliationKeywords, PublicationsUserKeyword, UsersSimilar, Cookies,
        UsersRelations, UsersAffKeywords, DialogAbstract, DialogWordsCloud, SendResults],
  entryComponents: [MakeComment, DialogAbstract, DialogWordsCloud, SendResults],
  providers : [WindowRefService, SearchIndivService, SearchPubService, SearchLabService, LocalStorageService, ClusteringService, CommentService, SendMailServive],
  bootstrap: [ AppComponent ]
})

export class AppModule {}
