import {Component, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {SearchLabService} from '../../services/es-lab-serv';

/**
* Use to display publication of affiliation by keywords
*/

@Component({
    selector : 'publications-lab-keywords',
    templateUrl : './affiliation-pub-keywords.html'
})

export class AffiliationKeywords implements OnInit{

    pageSize = {count: 10, label: '10 per page '};
    currentPage = 1;

    done : boolean = false;
    aff_done : boolean = false;
    id : string;
    keywords : string;
    pubFound : number;
    numPages : number;
    name : string;
    acronym : string;

    // List of all publications
    filteredPublications : any;
    // List of current publications to display
    currentPublications : any;
    // Nb result and time takes
    resultStat : any;

    constructor(private route : ActivatedRoute, private searchLabService : SearchLabService){}

    ngOnInit(){
        this.route.params.subscribe(params => {
            // Récupération des valeurs de l'URL
            this.id = params['id'];
            this.keywords = decodeURIComponent(params['keywords']);
        });
        this.getAffiliation();
    }

    /**
    * Get affiliation by Id
    */
    getAffiliation(){
        this.searchLabService.getAffiliationBastri(this.id).then(
            (resp) => {
                this.name = resp._source.name;
                this.acronym = resp._source.acronym;
                this.aff_done = true;
                this.setCurrentPublications();
            }
        );
    }

    /**
    * Select page to display for publication
    * @param page : number of the page
    */
    selectPage(page) {
        this.currentPage = page.numPage;
        this.setCurrentPublications();
    };

    /**
    * Select the new user with new select page size option
    * @param pageSize : nb user to display by page
    */
    selectPageSize(pageSize){
        if(this.currentPage * pageSize.pageSize.count > this.pubFound){
            this.currentPage = Math.ceil(this.pubFound / pageSize.pageSize.count)
        }
        this.pageSize = pageSize.pageSize;
        this.setCurrentPublications();
    };

    /**
    * Set publications to display
    */
    private setCurrentPublications() {
        let from = (this.currentPage - 1) * this.pageSize.count;
        if(!this.done){
            if(this.keywords.indexOf('"') != -1 && this.keywords.indexOf('"') != this.keywords.lastIndexOf('"') && this.keywords.split(" ").length > 1){
                let text = this.keywords;
                let re = /\"(.*?)\"/g;
                let newtext = text.match(re);
                let unigrams = "";
                let tri = "";
                let bi = "";
                for(let i = 0; i < newtext.length; i++){
                    let biortri = newtext[i];
                    unigrams = text.replace(biortri, '');
                    biortri = biortri.replace('"', '');
                    biortri = biortri.replace('"', '');
                    let biortri_split = newtext[i].split(" ");
                    biortri = biortri.replace('"', '');
                    biortri = biortri.replace('"', '');
                    if(biortri_split.length == 2){
                        bi = bi +" "+ biortri
                    }
                    if(biortri_split.length == 3){
                        tri = tri +" "+ biortri
                    }
                }
                tri = tri.trim();
                bi = bi.trim();
                // text = text.replace('"', '');
                // text = text.replace('"', '');
                if(bi != "" && tri != "" && unigrams.match(/[A-Za-z0-9]+/g)){
                    this.searchLabService.getPublicationBySignificantBigramsTrigramsText(from, this.pageSize.count, this.id, tri, bi, unigrams).then(
                        resp => {
                            if(resp.hits.hits[0].inner_hits.pubs.hits.total.value == 0){
                                this.searchLabService.getPublicationBySignificantKeywords(from, this.pageSize.count, this.id, this.keywords).then(
                                    resp => {
                                        this.filteredPublications = resp.hits.hits[0].inner_hits.pubs.hits.hits;
                                        this.pubFound = resp.hits.hits[0].inner_hits.pubs.hits.total.value;
                                        this.currentPublications = this.filteredPublications.slice(from, this.pageSize.count);
                                        this.numPages = Math.ceil(this.pubFound / this.pageSize.count);
                                        this.resultStat = {total: this.pubFound, took: resp.took};
                                        this.done = true;
                                    }
                                );
                            }
                            else{
                                this.filteredPublications = resp.hits.hits[0].inner_hits.pubs.hits.hits;
                                this.pubFound = resp.hits.hits[0].inner_hits.pubs.hits.total.value;
                                this.currentPublications = this.filteredPublications.slice(from, this.pageSize.count);
                                this.numPages = Math.ceil(this.pubFound / this.pageSize.count);
                                this.resultStat = {total: this.pubFound, took: resp.took};
                                this.done = true;
                            }
                        })
                }else if(bi != "" && tri != "" && !unigrams.match(/[A-Za-z0-9]+/g).length){
                    this.searchLabService.getPublicationBySignificantBigramsTrigrams(from, this.pageSize.count, this.id, tri, bi).then(
                        resp => {
                            if(resp.hits.hits[0].inner_hits.pubs.hits.total.value == 0){
                                this.searchLabService.getPublicationBySignificantKeywords(from, this.pageSize.count, this.id, this.keywords).then(
                                    resp => {
                                        this.filteredPublications = resp.hits.hits[0].inner_hits.pubs.hits.hits;
                                        this.pubFound = resp.hits.hits[0].inner_hits.pubs.hits.total.value;
                                        this.currentPublications = this.filteredPublications.slice(from, this.pageSize.count);
                                        this.numPages = Math.ceil(this.pubFound / this.pageSize.count);
                                        this.resultStat = {total: this.pubFound, took: resp.took};
                                        this.done = true;
                                    }
                                );
                            }
                            else{
                                this.filteredPublications = resp.hits.hits[0].inner_hits.pubs.hits.hits;
                                this.pubFound = resp.hits.hits[0].inner_hits.pubs.hits.total.value;
                                this.currentPublications = this.filteredPublications.slice(from, this.pageSize.count);
                                this.numPages = Math.ceil(this.pubFound / this.pageSize.count);
                                this.resultStat = {total: this.pubFound, took: resp.took};
                                this.done = true;
                            }
                        })
                }else if(bi == "" && tri != "" && unigrams.match(/[A-Za-z0-9]+/g)){
                    this.searchLabService.getPublicationBySignificantTrigramsText(from, this.pageSize.count, this.id, tri, unigrams).then(
                        resp => {
                            if(resp.hits.hits[0].inner_hits.pubs.hits.total.value == 0){
                                this.searchLabService.getPublicationBySignificantKeywords(from, this.pageSize.count, this.id, this.keywords).then(
                                    resp => {
                                        this.filteredPublications = resp.hits.hits[0].inner_hits.pubs.hits.hits;
                                        this.pubFound = resp.hits.hits[0].inner_hits.pubs.hits.total.value;
                                        this.currentPublications = this.filteredPublications.slice(from, this.pageSize.count);
                                        this.numPages = Math.ceil(this.pubFound / this.pageSize.count);
                                        this.resultStat = {total: this.pubFound, took: resp.took};
                                        this.done = true;
                                    }
                                );
                            }
                            else{
                                this.filteredPublications = resp.hits.hits[0].inner_hits.pubs.hits.hits;
                                this.pubFound = resp.hits.hits[0].inner_hits.pubs.hits.total.value;
                                this.currentPublications = this.filteredPublications.slice(from, this.pageSize.count);
                                this.numPages = Math.ceil(this.pubFound / this.pageSize.count);
                                this.resultStat = {total: this.pubFound, took: resp.took};
                                this.done = true;
                            }
                        })
                }else if(bi != "" && tri == "" && unigrams.match(/[A-Za-z0-9]+/g)){
                    this.searchLabService.getPublicationBySignificantTBigramsText(from, this.pageSize.count, this.id, bi, unigrams).then(
                        resp => {
                            if(resp.hits.hits[0].inner_hits.pubs.hits.total.value == 0){
                                this.searchLabService.getPublicationBySignificantKeywords(from, this.pageSize.count, this.id, this.keywords).then(
                                    resp => {
                                        this.filteredPublications = resp.hits.hits[0].inner_hits.pubs.hits.hits;
                                        this.pubFound = resp.hits.hits[0].inner_hits.pubs.hits.total.value;
                                        this.currentPublications = this.filteredPublications.slice(from, this.pageSize.count);
                                        this.numPages = Math.ceil(this.pubFound / this.pageSize.count);
                                        this.resultStat = {total: this.pubFound, took: resp.took};
                                        this.done = true;
                                    }
                                );
                            }
                            else{
                                this.filteredPublications = resp.hits.hits[0].inner_hits.pubs.hits.hits;
                                this.pubFound = resp.hits.hits[0].inner_hits.pubs.hits.total.value;
                                this.currentPublications = this.filteredPublications.slice(from, this.pageSize.count);
                                this.numPages = Math.ceil(this.pubFound / this.pageSize.count);
                                this.resultStat = {total: this.pubFound, took: resp.took};
                                this.done = true;
                            }
                        })
                }else if(bi != "" && tri == "" && !unigrams.match(/[A-Za-z0-9]+/g)){
                    this.searchLabService.getPublicationBySignificantBigrams(from, this.pageSize.count, this.id, bi).then(
                        resp => {
                            if(resp.hits.hits[0].inner_hits.pubs.hits.total.value == 0){
                                this.searchLabService.getPublicationBySignificantKeywords(from, this.pageSize.count, this.id, this.keywords).then(
                                    resp => {
                                        this.filteredPublications = resp.hits.hits[0].inner_hits.pubs.hits.hits;
                                        this.pubFound = resp.hits.hits[0].inner_hits.pubs.hits.total.value;
                                        this.currentPublications = this.filteredPublications.slice(from, this.pageSize.count);
                                        this.numPages = Math.ceil(this.pubFound / this.pageSize.count);
                                        this.resultStat = {total: this.pubFound, took: resp.took};
                                        this.done = true;
                                    }
                                );
                            }
                            else{
                                this.filteredPublications = resp.hits.hits[0].inner_hits.pubs.hits.hits;
                                this.pubFound = resp.hits.hits[0].inner_hits.pubs.hits.total.value;
                                this.currentPublications = this.filteredPublications.slice(from, this.pageSize.count);
                                this.numPages = Math.ceil(this.pubFound / this.pageSize.count);
                                this.resultStat = {total: this.pubFound, took: resp.took};
                                this.done = true;
                            }
                        })
                }else if(bi == "" && tri != "" && !unigrams.match(/[A-Za-z0-9]+/g)){
                    this.searchLabService.getPublicationBySignificantTrigrams(from, this.pageSize.count, this.id, tri).then(
                        resp => {
                            if(resp.hits.hits[0].inner_hits.pubs.hits.total.value == 0){
                                this.searchLabService.getPublicationBySignificantKeywords(from, this.pageSize.count, this.id, this.keywords).then(
                                    resp => {
                                        this.filteredPublications = resp.hits.hits[0].inner_hits.pubs.hits.hits;
                                        this.pubFound = resp.hits.hits[0].inner_hits.pubs.hits.total.value;
                                        this.currentPublications = this.filteredPublications.slice(from, this.pageSize.count);
                                        this.numPages = Math.ceil(this.pubFound / this.pageSize.count);
                                        this.resultStat = {total: this.pubFound, took: resp.took};
                                        this.done = true;
                                    }
                                );
                            }
                            else{
                                this.filteredPublications = resp.hits.hits[0].inner_hits.pubs.hits.hits;
                                this.pubFound = resp.hits.hits[0].inner_hits.pubs.hits.total.value;
                                this.currentPublications = this.filteredPublications.slice(from, this.pageSize.count);
                                this.numPages = Math.ceil(this.pubFound / this.pageSize.count);
                                this.resultStat = {total: this.pubFound, took: resp.took};
                                this.done = true;
                            }
                        })
                }
                // let text = this.keywords.replace('"', '');
                // text = text.replace('"', '');
                // if(text.split(" ").length == 2){
                // this.searchLabService.getPublicationBySignificantBigrams(from, this.pageSize.count, this.id, text).then(
                //     resp => {
                //         if(resp.hits.hits[0].inner_hits.pubs.hits.total.value == 0){
                //             this.searchLabService.getPublicationBySignificantKeywords(from, this.pageSize.count, this.id, this.keywords).then(
                //                 resp => {
                //                     this.filteredPublications = resp.hits.hits[0].inner_hits.pubs.hits.hits;
                //                     this.pubFound = resp.hits.hits[0].inner_hits.pubs.hits.total.value;
                //                     this.currentPublications = this.filteredPublications.slice(from, this.pageSize.count);
                //                     this.numPages = Math.ceil(this.pubFound / this.pageSize.count);
                //                     this.resultStat = {total: this.pubFound, took: resp.took};
                //                     this.done = true;
                //                 }
                //             );
                //         }
                //         else{
                //             this.filteredPublications = resp.hits.hits[0].inner_hits.pubs.hits.hits;
                //             this.pubFound = resp.hits.hits[0].inner_hits.pubs.hits.total.value;
                //             this.currentPublications = this.filteredPublications.slice(from, this.pageSize.count);
                //             this.numPages = Math.ceil(this.pubFound / this.pageSize.count);
                //             this.resultStat = {total: this.pubFound, took: resp.took};
                //             this.done = true;
                //         }
                //     }
                // );
                // }else{
                //     this.searchLabService.getPublicationBySignificantTrigrams(from, this.pageSize.count, this.id, text).then(
                //         resp => {
                //             if(resp.hits.hits[0].inner_hits.pubs.hits.total.value == 0){
                //                 this.searchLabService.getPublicationBySignificantKeywords(from, this.pageSize.count, this.id, this.keywords).then(
                //                     resp => {
                //                         this.filteredPublications = resp.hits.hits[0].inner_hits.pubs.hits.hits;
                //                         this.pubFound = resp.hits.hits[0].inner_hits.pubs.hits.total.value;
                //                         this.currentPublications = this.filteredPublications.slice(from, this.pageSize.count);
                //                         this.numPages = Math.ceil(this.pubFound / this.pageSize.count);
                //                         this.resultStat = {total: this.pubFound, took: resp.took};
                //                         this.done = true;
                //                     }
                //                 );
                //             }
                //             else{
                //                 this.filteredPublications = resp.hits.hits[0].inner_hits.pubs.hits.hits;
                //                 this.pubFound = resp.hits.hits[0].inner_hits.pubs.hits.total.value;
                //                 this.currentPublications = this.filteredPublications.slice(from, this.pageSize.count);
                //                 this.numPages = Math.ceil(this.pubFound / this.pageSize.count);
                //                 this.resultStat = {total: this.pubFound, took: resp.took};
                //                 this.done = true;
                //             }
                //         }
                //     );
                // }
            }else{
                this.searchLabService.getPublicationBySignificantKeywords(from, this.pageSize.count, this.id, this.keywords).then(
                    resp => {
                        this.filteredPublications = resp.hits.hits[0].inner_hits.pubs.hits.hits;
                        this.pubFound = resp.hits.hits[0].inner_hits.pubs.hits.total.value;
                        this.currentPublications = this.filteredPublications.slice(from, this.pageSize.count);
                        this.numPages = Math.ceil(this.pubFound / this.pageSize.count);
                        this.resultStat = {total: this.pubFound, took: resp.took};
                        this.done = true;
                    }
                );
            }
        }else{
            this.done = false;
            let to = from + this.pageSize.count;
            this.currentPublications = this.filteredPublications.slice(from, to);
            this.numPages = Math.ceil(this.pubFound / this.pageSize.count);
            this.done = true;
        }
    };

    /**
    * Check if allPublication exist
    * @returns {boolean}
    */
    isAvailableResults() {
        return this.currentPublications ? true : false;
    };

    /**
    * Check if at least 1 publication is available
    * @returns {boolean}
    */
    isAtLeastOneResult() {
        if (!this.isAvailableResults()) {
            return false;
        }
        return this.currentPublications.length > 0;
    };


    /**
    * Check if mobile
    * @returns {boolean} : return true if mobile
    */
    mobileCheck() {
        let check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor);
        return check;
    };

}
