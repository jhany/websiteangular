import { NgModule }            from '@angular/core';
import { RouterModule }        from '@angular/router';


import { AffiliationKeywords } from './publication-lab-keywords';

@NgModule({
  imports: [RouterModule.forChild([
    { path: ':id/:keywords', component: AffiliationKeywords},
  ])],
  exports: [RouterModule]
})
export class AffiliationKeywordRoutingModule {}
