import { NgModule }  from '@angular/core';
import { CommonModule }  from '@angular/common';

import { SharedModule }       from '../shared.module';


//providers
import {SearchPubService} from '../services/es-pub-serv';

@NgModule({
  imports: [CommonModule, SharedModule],
  declarations : [],
  providers : [ SearchPubService],
})

export class PublicationModule {}
