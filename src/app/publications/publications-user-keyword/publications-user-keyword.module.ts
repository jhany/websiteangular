import { NgModule }  from '@angular/core';
import { CommonModule }  from '@angular/common';
import { SharedModule } from '../../shared.module';
import {  PublicationModule} from '../publications.module';
import {PublicationsUserKeywordRoutingModule } from './publications-user-keywords-routing.module';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from "@angular/flex-layout";

import { PublicationsUserKeyword } from './publications-user-keywords';

@NgModule({
  imports: [CommonModule, SharedModule, PublicationModule, PublicationsUserKeywordRoutingModule, MaterialModule, FormsModule, FlexLayoutModule],
  declarations: [PublicationsUserKeyword]
})

export class PublicationsUserKeywordsModule {}
