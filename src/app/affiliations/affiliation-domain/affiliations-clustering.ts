import {Component, OnChanges, SimpleChange, OnInit, Inject, Input} from '@angular/core';
import {ClusteringService} from '../../services/clustering_service';

/*
*    Component to display publications in list
*/
@Component({
    selector : 'affiliation-clustering',
    templateUrl : '../../profile/profile-clustering/profile-clustering.html'
})

export class AffiliationClustering implements OnInit{

    @Input() acronym;
    publications_lingo = {};
    words_lingo = [];
    done = false;

    constructor(private clusterService : ClusteringService){}

    ngOnInit(){
        this.getLingoTopics(this.acronym);
    }

    /**
    * Get clusters with lingo algorithm
    * @param acronym : acronym of the team
    **/
    private getLingoTopics(acronym){
      this.clusterService.getlingo_cluster_team(acronym).then(
        resp => {
          let res : any = resp;
          let label :any;
          for(label of res.clusters){
            this.publications_lingo[label.label] = label.documents
            this.words_lingo.push(label.label)
            this.done = true;
          }

        }
      )
    }

    gotoArticle(){
      window.open("https://link.springer.com/chapter/10.1007/978-3-540-39985-8_37", '_blank');
    }
}
