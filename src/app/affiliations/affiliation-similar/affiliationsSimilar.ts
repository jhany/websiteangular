import {Component, Input, OnInit} from '@angular/core';
import {SearchLabService} from '../../services/es-lab-serv';
import { ActivatedRoute } from '@angular/router';

/**
 *  Affiliation Search controller
 *  Use to search affiliation by keywords give by the user
 */

  @Component({
      selector : 'affiliations-similar',
      templateUrl : './affiliations-similar.html'
  })

export class AffiliationsSimilar implements OnInit{

    // Variable initialization

    aff_number = 0;

    pageSize = {count: 10, label: "10 per page"};

    searchText = "";

    similarAffiliation = [];
    searchResphit = {};
    done = false;

    key_lab;
    origin;
    name;
    acronym;
    currentPage;
    numPages;
    researchStart = false;
    researchProcess;
    searchResp;
    listSearchResp;
    resultStat;

    constructor(private route : ActivatedRoute, private searchLabServive : SearchLabService){}

    ngOnInit(){
        this.route.params.subscribe(params => {
            // Récupération des valeurs de l'URL
            this.key_lab = params['id'];
        });
        this.getAffiliation();
        this.getPubAffiliation(this.key_lab, 1);
    }

    /**
     * Get affiliation by Id
     */
     getAffiliation(){
        this.searchLabServive.getAffiliationBastri(this.key_lab).then(
            resp => {
                this.origin = resp._source;
                this.name = resp._source.name;
                this.acronym = resp._source.acronym;
            }
        );
    }

    /**
     * Select the page to display
     * @param page : number of the page to display
     */
    selectPage(page) {
        this.currentPage = page.numPage;
        this.getPubAffiliation(this.key_lab, page.numPage);
    };

    /**
     * Select the new user with new select page size option
     * @param pageSize : number of users to display by page
     */
    selectPageSize(pageSize){
        this.pageSize = pageSize.pageSize;
        if(this.currentPage * pageSize.pageSize.count > this.aff_number){
            this.numPages = Math.ceil(this.similarAffiliation.length / this.pageSize.count);
            this.currentPage = Math.ceil(this.aff_number / pageSize.pageSize.count);
        }
        this.getPubAffiliation(this.key_lab, this.currentPage);
    };

    /**
     * Use to get similar Affiliation (get publication of affiliation and find other affiliation in publication)
     * @param affiliationId : id of the affiliation
     * @param page : number of the page to display
     */
    getPubAffiliation(affiliationId, page){
        this.currentPage = page;
        this.researchProcess = true;
        this.done = false;
        var from = (page - 1) * this.pageSize.count;
        this.searchLabServive.getSimilarAffiliation(from, this.pageSize.count, affiliationId).then(
         (resp) => {
                if (resp.hits.total.value == 0) {
                    this.researchStart = true;
                    this.researchProcess = false;
                    this.done = true;
                } else {
                    this.researchStart = false;
                    this.aff_number = resp.hits.total.value;
                    this.searchResphit = resp;
                    this.searchResp = resp;
                    this.listSearchResp = resp.hits.hits;
                    this.resultStat = {total: resp.hits.total.value, took: resp.took};
                    //Get location if exist and place them on map
                    //placeAffiliation(this.searchResphit.hits.hits);
                    this.numPages = Math.ceil(this.aff_number / this.pageSize.count);
                    this.researchProcess = false;
                    this.done = true;
                }
            });
    }

    /**
     *  isAvailableResults : check if results
     *  @return {boolean}
     *
     */
    isAvailableResults() {
        return this.searchResp ? true : false;
    };

    /**
     * isAtLeastOneResult : check if at least one result
     * @returns {boolean}
     */
    isAtLeastOneResult() {
        if (!this.isAvailableResults()) {
            return false;
        }
        return this.searchResp.hits.hits.length > 0;
    };

    /**
     * Use to check if is a mobile or not to display list instead of cards
     * @returns {boolean} : true if is a mobile
     */
    mobileCheck() {
        var check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor);
        return check;
    };

}
