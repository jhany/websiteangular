import { Injectable } from '@angular/core';
import * as elastic from 'elasticsearch-browser';
import { ElasticService } from './es-serv';
import {Globals} from '../globals'
/**
* All the functionality to get or search affiliation or publication in Elasticsearch
*/
@Injectable()
export class SearchIndivService{

    client : elastic.Client;
    private globals : Globals = new Globals();

    constructor(){
        this.client = new ElasticService().getClient();
    }
    // Get user
    getUser(userId) {
        return this.client.get<any>({
            index: this.globals.index_author,
            type: this.globals.doc_type_author,
            id: userId
        });
    }

    // Get the users similar to a given user
    getSimilarUsers(from, size, userId) {
        return this.client.search({
            index: this.globals.index_author,
            type: this.globals.doc_type_author,
            body: {
                "from": from,
                "size": size,
                "query": {
                    "nested": {
                        "path": "pubs",
                        "score_mode": "sum",
                        "query": {
                            "bool" :{
                                "should" : [
                                     {"more_like_this": {
                                        "fields": ["pubs.title_en.trigrams_sw"],
                                        "like": [{
                                            "_id" : userId
                                        }],
                                        "min_term_freq": 2,
                                        "min_word_length": 4,
                                        "max_query_terms" : 25,
                                        "boost_terms" : 10,
                                    "minimum_should_match" : "30%"
                                    }},
                                    {"more_like_this": {
                                       "fields": ["pubs.keywords_en"],
                                       "like": [{
                                           "_id" : userId
                                       }],
                                       "min_term_freq": 2,
                                       "min_word_length": 4,
                                       "max_query_terms" : 20,
                                       "boost_terms" : 5,
                                   "minimum_should_match" : "30%"
                                   }},
                                    {"more_like_this": {
                                        "fields": ["pubs.abstract_en.trigrams_sw"],
                                        "like": [{
                                            "_id" : userId
                                        }],
                                        "min_term_freq": 2,
                                        "min_word_length": 4,
                                        "max_query_terms" : 25,
                                        "boost_terms" : 5,
                                    "minimum_should_match" : "30%"
                                    }}
                                ]}
                            }
                        }
                    }
                }
            });
        }

        // Get the users similar to a given user
        getSimilarUsersExplain(userId, user_explain) {
            return this.client.search({
                index: this.globals.index_author,
                type: this.globals.doc_type_author,
                body: {
                    "query": {
                        "bool" : {
                            "must" :[ {
                                    "ids" : { "values" : [user_explain] }
                            },
                            {
                        "nested": {
                            "path": "pubs",
                            "score_mode": "sum",
                            "query": {
                                "bool" :{
                                    "should" : [
                                         {"more_like_this": {
                                            "fields": ["pubs.title_en.trigrams_sw"],
                                            "like": [{
                                                "_id" : userId
                                            }],
                                            "min_term_freq": 2,
                                            "min_word_length": 4,
                                            "max_query_terms" : 25,
                                            "boost_terms" : 10,
                                        "minimum_should_match" : "30%"
                                        }},
                                        {"more_like_this": {
                                           "fields": ["pubs.keywords_en"],
                                           "like": [{
                                               "_id" : userId
                                           }],
                                           "min_term_freq": 2,
                                           "min_word_length": 4,
                                           "max_query_terms" : 20,
                                           "boost_terms" : 5,
                                       "minimum_should_match" : "30%"
                                       }},
                                        {"more_like_this": {
                                            "fields": ["pubs.abstract_en.trigrams_sw"],
                                            "like": [{
                                                "_id" : userId
                                            }],
                                            "min_term_freq": 2,
                                            "min_word_length": 4,
                                            "max_query_terms" : 25,
                                            "boost_terms" : 5,
                                        "minimum_should_match" : "30%"
                                        }}
                                    ]}
                                },
                                "inner_hits": {
                                    "_source": false,
                                    "explain": true
                                }
                            }
                        }
                    ]
                }
            }
                        }
                });
            }

        // Get the users similar to a given user
        getSimilarUsers10(from, size, userId) {
            return this.client.search({
                index: this.globals.index_author,
                type: this.globals.doc_type_author,
                body: {
                    "from": from,
                    "size": size,
                    "query": {
                        "nested": {
                            "path": "pubs",
                            "score_mode": "sum",
                            "query": {
                                "bool" :{
                                    "should" : [
                                        {"more_like_this": {
                                            "fields": ["pubs.title_en"],
                                            "like": [{
                                                "_id" : userId
                                            }],
                                            "min_term_freq": 1,
                                            "min_word_length": 4,
                                            "max_query_terms" : 30
                                        }},
                                        {"more_like_this": {
                                            "fields": ["pubs.title_fr"],
                                            "like": [{
                                                "_id" : userId
                                            }],
                                            "min_term_freq": 1,
                                            "min_word_length": 4,
                                            "max_query_terms" : 30
                                        }},
                                        {"more_like_this": {
                                            "fields": ["pubs.abstract_en"],
                                            "like": [{
                                                "_id" : userId
                                            }],
                                            "min_term_freq": 2,
                                            "min_word_length": 4,
                                            "max_query_terms" : 30
                                        }},
                                        {"more_like_this": {
                                            "fields": ["pubs.abstract_fr"],
                                            "like": [{
                                                "_id" : userId
                                            }],
                                            "min_term_freq": 2,
                                            "min_word_length": 4,
                                            "max_query_terms" : 30
                                        }},
                                        {"more_like_this": {
                                            "fields": ["pubs.keywords_en"],
                                            "like": [{
                                                "_id" : userId
                                            }],
                                            "min_term_freq": 1,
                                            "min_word_length": 4,
                                            "max_query_terms" : 30
                                        }},
                                        {"more_like_this": {
                                            "fields": ["pubs.keywords_fr"],
                                            "like": [{
                                                "_id" : userId
                                            }],
                                            "min_term_freq": 1,
                                            "min_word_length": 4,
                                            "max_query_terms" : 30
                                        }}
                                    ]}
                                },
                                "inner_hits": {
                                    "_source": false,
                                    "explain": true
                                }
                            }
                        }
                    }
                });
            }

        getUserAffiliation(from, size, affiliationId, textsearch){
            return this.client.search({
                index: this.globals.index_author,
                type: this.globals.doc_type_author,
                body: {
                    "from": from,
                    "size": size,
                    "query": {
                        "bool" : {
                            "must" : [
                                {
                                    "nested" : {
                                        "path" : "history",
                                        "query" : {
                                    "match": {
                                        "history.structure": affiliationId
                                    }
                                    }
                                }

                                },
                                {
                                    "nested": {
                                        "path": "pubs",
                                        "score_mode" : "sum",
                                        "query": {
                                            "function_score": {
                                                "script_score": {
                                                    "script": {
                                                        "lang": "painless",
                                                        "inline": "_score * _score * _score * _score"
                                                    }
                                                },
                                                "boost_mode": "multiply",
                                                "query": {
                                                    "bool": {
                                                        "should": [
                                                            {
                                                                "multi_match": {
                                                                    "fields": [
                                                                        "pubs.abstract_en",
                                                                        "pubs.abstract_fr"
                                                                    ],
                                                                    "query": textsearch,
                                                                    "boost": 2
                                                                }
                                                            },
                                                            {
                                                                "multi_match": {
                                                                    "fields": [
                                                                        "pubs.title_en",
                                                                        "pubs.title_fr"
                                                                    ],
                                                                    "query" : textsearch,
                                                                    "boost": 4
                                                                }
                                                            },
                                                            {
                                                                "multi_match": {
                                                                    "query" : textsearch,
                                                                    "fields" : ["pubs.keywords_*"],
                                                                    "type":       "most_fields",
                                                                    "boost": 10
                                                                }
                                                            },
                                                            {
                                                                "multi_match": {
                                                                    "query" : textsearch,
                                                                    "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams"],
                                                                    "type":       "most_fields",
                                                                    "boost": 150
                                                                }
                                                            },
                                                            {
                                                                "multi_match": {
                                                                    "query" : textsearch,
                                                                    "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams"],
                                                                    "type":       "most_fields",
                                                                    "boost": 200
                                                                }
                                                            }
                                                        ]

                                                    }
                                                }
                                            }
                                        },
                                        "inner_hits": {
                                            "explain" : true,
                                            "_source": false
                                        }
                                    }
                                }
                            ]
                        }
                    }
                }
            });
        }

        // Get the competencies related to a given text
        /**
        * Use to get the user when search by competencies
        * Get the competencies related to a given text
            "bool" : {
                "should" : [
                    {"match" : {"forename" : competencies}},
                    {"match" : {"surname" : competencies}},
        * works with parent/child
        * @param from : first user to show
        * @param size : number of user to show
        * @param competencies : text to search
        * @returns {*} : the user find by the query
        */
        userTextSearchInRange(from, size, competencies) {
            return this.client.search({
                index: this.globals.index_author,
                type: this.globals.doc_type_author,
                body: {
                    "from": from,
                    "size": size,
                    "query" : {
                        "nested": {
                            "path": "pubs",
                            "score_mode": "sum",
                            "query": {
                                "function_score" : {
                                    "score_mode": "sum",
                                    "script_score" : {
                                        "script" : {
                                            "lang": "painless",
                                            "inline" : "Math.pow(_score, 4)"
                                        }
                                    },
                                    "boost_mode":"replace",
                                    "query" : {
                                        "bool": {
                                            "should": [
                                                {
                                                    "multi_match": {
                                                        "fields": [
                                                            "pubs.abstract_en",
                                                            "pubs.abstract_fr"
                                                        ],
                                                        "query": competencies,
                                                        "boost": 1
                                                    }
                                                },
                                                {
                                                    "multi_match": {
                                                        "fields": [
                                                            "pubs.title_en",
                                                            "pubs.title_fr"
                                                        ],
                                                        "query" : competencies,
                                                        "boost": 4
                                                    }
                                                },
                                                {
                                                    "multi_match": {
                                                        "query" : competencies,
                                                        "fields" : ["pubs.keywords_*"],
                                                        "type":       "most_fields",
                                                        "boost": 10
                                                    }
                                                },
                                                {
                                                    "multi_match": {
                                                        "query" : competencies,
                                                        "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams"],
                                                        "type":       "most_fields",
                                                        "boost": 50
                                                    }
                                                },
                                                {
                                                    "multi_match": {
                                                        "query" : competencies,
                                                        "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams"],
                                                        "type":       "most_fields",
                                                        "boost": 100
                                                    }
                                                }
                                            ]
                                        }
                                    }
                                }
                            },
                                    "inner_hits": {
                                        "explain": true
                                    }

        }
                }
            }
            });

        }

        userTextSearchInRangeBigrams(from, size, bigrams) {
            let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
            let bigrams_clean_en = [];
            let bigrams_clean_fr = [];
            let newtext = bigrams.match(re_bi);
            for(var i = 0; i < newtext.length; i++){
                bigrams_clean_en[i] = {
                        "match_phrase" : {
                        "pubs.keywords_en" : {
                            "query" : newtext[i],
                            "boost" : 2
                        }
                    }
                };
                bigrams_clean_fr[i] = {
                        "match_phrase" : {
                        "pubs.keywords_fr" : {
                            "query" : newtext[i],
                            "boost" : 2
                        }
                    }
                };
            }
            return this.client.search({
                index: this.globals.index_author,
                type: this.globals.doc_type_author,
                body: {
                    "from": from,
                    "size": size,
                    "query" : {
                        "nested": {
                            "path": "pubs",
                            "score_mode": "sum",
                            "query": {
                                        "bool": {
                                            "should": [
                                                {
                                                    "multi_match" : {
                                                        "query" : bigrams,
                                                        "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams"],
                                                        "boost" : 2
                                                    }
                                                },
                                                {
                                                    "multi_match" : {
                                                        "query" : bigrams,
                                                        "fields" : ["pubs.abstract_en.bigrams", "pubs.abstract_fr.bigrams"]
                                                    }
                                                },
                                                bigrams_clean_en,
                                                bigrams_clean_fr

                                            ]
                                        }
                            },
                            "inner_hits" : {
                                "explain" : true
                            }
                        }
                    }

                }

            });
        }
        userTextSearchInRangeTrigrams(from, size, trigrams) {
            let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
            let trigrams_clean_en = [];
            let trigrams_clean_fr = [];
            let newtexttri = trigrams.match(re_tri);
            for(var i = 0; i < newtexttri.length; i++){
                trigrams_clean_en[i] = {
                        "match_phrase" : {
                        "pubs.keywords_en" : {
                            "query" : newtexttri[i],
                            "boost" : 2
                        }
                    }
                };
                trigrams_clean_fr[i] = {
                        "match_phrase" : {
                        "pubs.keywords_fr" : {
                            "query" : newtexttri[i],
                            "boost" : 2
                        }
                    }
                };
            }
            return this.client.search({
                index: this.globals.index_author,
                type: this.globals.doc_type_author,
                body: {
                    "from": from,
                    "size": size,
                    "query" : {
                        "nested": {
                            "path": "pubs",
                            "score_mode": "sum",
                            "query": {
                                        "bool": {
                                            "should": [
                                                {
                                                    "multi_match" : {
                                                        "query" : trigrams,
                                                        "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams"],
                                                        "boost" : 2
                                                    }
                                                },
                                                {
                                                    "multi_match" : {
                                                        "query" : trigrams,
                                                        "fields" : ["pubs.abstract_en.trigrams", "pubs.abstract_fr.trigrams"]
                                                    }
                                                },
                                                trigrams_clean_en,
                                                trigrams_clean_fr
                                            ]

                                }
                            },
                            "inner_hits" : {
                                "explain" : true
                            }
                        }
                    }

                }

            });
        }
        userTextSearchInRangeBigramsKeywords(from, size, text) {
            return this.client.search({
                index: this.globals.index_author,
                type: this.globals.doc_type_author,
                body: {
                    "from": from,
                    "size": size,
                    "query" : {
                        "nested": {
                            "path": "pubs",
                            "score_mode": "avg",
                            "query": {
                                "function_score" : {
                                    "script_score" : {
                                        "script" : {
                                            "lang": "painless",
                                            "inline" : "Math.pow(_score, 10)"
                                        }
                                    },
                                    "boost_mode":"replace",
                                    "query" : {
                                        "bool": {
                                            "should": [
                                                {
                                                    "multi_match": {
                                                        "query" : text,
                                                        "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams"],
                                                        "type":       "most_fields"
                                                    }
                                                },
                                                {
                                                    "multi_match": {
                                                        "query" : text,
                                                        "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams"],
                                                        "type":       "most_fields",
                                                        "boost" : 2
                                                    }
                                                }
                                            ],
                                            "must" : [
                                                {
                                                    "match" : {"pubs.title_abstract" : text}
                                                }
                                            ]
                                        }
                                    }
                                }
                            },
                            "inner_hits" : {
                                "explain" : true
                            }
                        }
                    }

                }

            });

        }
        getPublicationsByUserAndKeyword(userId, text) {
            return this.client.search({
                index: this.globals.index_author,
                type: this.globals.doc_type_author,
                body: {
                    "size": 1,
                    "query": {
                        "bool": {
                            "must": [
                                {
                                    "ids" : {
                                        "values" : [userId]
                                    }
                                },
                                {
                                    "nested": {
                                        "path": "pubs",
                                        "query":
                                        {
                                            "match": {
                                                "pubs.title_abstract": text
                                            }
                                        }
                                        ,
                                        "inner_hits": {
                                            "size": 100
                                        }
                                    }
                                }

                            ]
                        }
                    }
                }
            });
        }
        getPublicationsByUserAndBigrams(userId, bigrams) {
            let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
            let bigrams_clean_en = [];
            let bigrams_clean_fr = [];
            let newtext = bigrams.match(re_bi);
            for(var i = 0; i < newtext.length; i++){
                bigrams_clean_en[i] = {
                        "match_phrase" : {
                        "pubs.keywords_en" : {
                            "query" : newtext[i],
                            "boost" : 2
                        }
                    }
                };
                bigrams_clean_fr[i] = {
                        "match_phrase" : {
                        "pubs.keywords_fr" : {
                            "query" : newtext[i],
                            "boost" : 2
                        }
                    }
                };
            }
            return this.client.search({
                index: this.globals.index_author,
                type: this.globals.doc_type_author,
                body: {
                    "size": 1,
                    "query": {
                        "bool": {
                            "must": [
                                {
                                    "ids" : {
                                        "values" : [userId]
                                    }
                                },
                                {
                                    "nested": {
                                        "path": "pubs",
                                        "query":{
                                        "bool": {
                                            "should": [
                                            {"multi_match" : {
                                                "query" : bigrams,
                                                "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams", "pubs.abstract_en.bigrams", "pubs.abstract_fr.bigrams"]
                                            }},
                                            bigrams_clean_fr,
                                            bigrams_clean_en
                                        ]}}
                                        ,
                                        "inner_hits": {
                                            "size": 100
                                        }
                                    }
                                }

                            ]
                        }
                    }
                }
            });
        }
        getPublicationsByUserAndTrigrams(userId, trigrams) {
            let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
            let trigrams_clean_en = [];
            let trigrams_clean_fr = [];
            let newtexttri = trigrams.match(re_tri);
            for(var i = 0; i < newtexttri.length; i++){
                trigrams_clean_en[i] = {
                        "match_phrase" : {
                        "pubs.keywords_en" : {
                            "query" : newtexttri[i],
                            "boost" : 2
                        }
                    }
                };
                trigrams_clean_fr[i] = {
                        "match_phrase" : {
                        "pubs.keywords_fr" : {
                            "query" : newtexttri[i],
                            "boost" : 2
                        }
                    }
                };
            }
            return this.client.search({
                index: this.globals.index_author,
                type: this.globals.doc_type_author,
                body: {
                    "size": 1,
                    "query": {
                        "bool": {
                            "must": [
                                {
                                    "ids" : {
                                        "values" : [userId]
                                    }
                                },
                                {
                                    "nested": {
                                        "path": "pubs",
                                        "query":{
                                        "bool": {
                                            "should": [
                                            {"multi_match" : {
                                                "query" : trigrams,
                                                "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams", "pubs.abstract_en.trigrams", "pubs.abstract_fr.trigrams"]
                                            }},
                                            trigrams_clean_en,
                                            trigrams_clean_fr
                                        ]}}
                                        ,
                                        "inner_hits": {
                                            "size": 100
                                        }
                                    }
                                }

                            ]
                        }
                    }
                }
            });
        }
        getPublicationsByUserAndTrigramsBigrams(userId, trigrams, bigrams) {
            let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
            let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
            let trigrams_clean_en = [];
            let trigrams_clean_fr = [];
            let bigrams_clean_en = [];
            let bigrams_clean_fr = [];
            let newtextbi = bigrams.match(re_bi);
            let newtexttri = trigrams.match(re_tri);
            for(var i = 0; i < newtextbi.length; i++){
                bigrams_clean_en[i] = {
                        "match_phrase" : {
                        "pubs.keywords_en" : {
                            "query" : newtextbi[i],
                            "boost" : 2
                        }
                    }
                };
                bigrams_clean_fr[i] = {
                        "match_phrase" : {
                        "pubs.keywords_fr" : {
                            "query" : newtextbi[i],
                            "boost" : 2
                        }
                    }
                };
            }
            for(var i = 0; i < newtexttri.length; i++){
                trigrams_clean_en[i] = {
                        "match_phrase" : {
                        "pubs.keywords_en" : {
                            "query" : newtexttri[i],
                            "boost" : 2
                        }
                    }
                };
                trigrams_clean_fr[i] = {
                        "match_phrase" : {
                        "pubs.keywords_fr" : {
                            "query" : newtexttri[i],
                            "boost" : 2
                        }
                    }
                };
            }
            return this.client.search({
                index: this.globals.index_author,
                type: this.globals.doc_type_author,
                body: {
                    "size": 1,
                    "query": {
                        "bool": {
                            "must": [
                                {
                                    "ids" : {
                                        "values" : [userId]
                                    }
                                },
                                {
                                    "nested": {
                                        "path": "pubs",
                                        "query":{
                                        "bool": {
                                            "should": [
                                            {"multi_match" : {
                                                "query" : trigrams,
                                                "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams", "pubs.abstract_en.trigrams", "pubs.abstract_fr.trigrams"]
                                            }},,
                                            {"multi_match" : {
                                                "query" : bigrams,
                                                "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams", "pubs.abstract_en.bigrams", "pubs.abstract_fr.bigrams"]
                                            }},
                                            bigrams_clean_fr,
                                            bigrams_clean_en,
                                            trigrams_clean_fr,
                                            trigrams_clean_en
                                        ]}}
                                        ,
                                        "inner_hits": {
                                            "size": 100,
                                            "explain": true
                                        }
                                    }
                                }

                            ]
                        }
                    }
                }
            });
        }
        getPublicationsByUserAndTrigramsBigramsText(userId, trigrams, bigrams, unigrams) {
            let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
            let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
            let trigrams_clean_en = [];
            let trigrams_clean_fr = [];
            let bigrams_clean_en = [];
            let bigrams_clean_fr = [];
            let newtextbi = bigrams.match(re_bi);
            let newtexttri = trigrams.match(re_tri);
            for(var i = 0; i < newtextbi.length; i++){
                bigrams_clean_en[i] = {
                        "match_phrase" : {
                        "pubs.keywords_en" : {
                            "query" : newtextbi[i],
                            "boost" : 2
                        }
                    }
                };
                bigrams_clean_fr[i] = {
                        "match_phrase" : {
                        "pubs.keywords_fr" : {
                            "query" : newtextbi[i],
                            "boost" : 2
                        }
                    }
                };
            }
            for(var i = 0; i < newtexttri.length; i++){
                trigrams_clean_en[i] = {
                        "match_phrase" : {
                        "pubs.keywords_en" : {
                            "query" : newtexttri[i],
                            "boost" : 2
                        }
                    }
                };
                trigrams_clean_fr[i] = {
                        "match_phrase" : {
                        "pubs.keywords_fr" : {
                            "query" : newtexttri[i],
                            "boost" : 2
                        }
                    }
                };
            }
            return this.client.search({
                index: this.globals.index_author,
                type: this.globals.doc_type_author,
                body: {
                    "size": 1,
                    "query": {
                        "bool": {
                            "must": [
                                {
                                    "ids" : {
                                        "values" : [userId]
                                    }
                                },
                                {
                                    "nested": {
                                        "path": "pubs",
                                        "query":{
                                        "bool": {
                                            "should": [
                                            {"multi_match" : {
                                                "query" : trigrams,
                                                "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams", "pubs.abstract_en.trigrams", "pubs.abstract_fr.trigrams"]
                                            }},
                                            trigrams_clean_fr,
                                            trigrams_clean_en,
                                            {"multi_match" : {
                                                "query" : bigrams,
                                                "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams", "pubs.abstract_en.bigrams", "pubs.abstract_fr.bigrams"]
                                            }},
                                            bigrams_clean_en,
                                            bigrams_clean_fr,
                                            {"multi_match" : {
                                                "query" : unigrams,
                                                "fields" : ["pubs.title_en", "pubs.title_fr", "pubs.abstract_en", "pubs.abstract_fr", "pubs.keywords_en", "pubs.keywords_fr"]
                                            }}
                                        ]}}
                                        ,
                                        "inner_hits": {
                                            "size": 100,
                                            "explain": true
                                        }
                                    }
                                }

                            ]
                        }
                    }
                }
            });
        }
        getPublicationsByUserAndTrigramsText(userId, trigrams, unigrams) {
            let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
            let trigrams_clean_en = [];
            let trigrams_clean_fr = [];
            let newtexttri = trigrams.match(re_tri);
            for(var i = 0; i < newtexttri.length; i++){
                trigrams_clean_en[i] = {
                        "match_phrase" : {
                        "pubs.keywords_en" : {
                            "query" : newtexttri[i],
                            "boost" : 2
                        }
                    }
                };
                trigrams_clean_fr[i] = {
                        "match_phrase" : {
                        "pubs.keywords_fr" : {
                            "query" : newtexttri[i],
                            "boost" : 2
                        }
                    }
                };
            }
            return this.client.search({
                index: this.globals.index_author,
                type: this.globals.doc_type_author,
                body: {
                    "size": 1,
                    "query": {
                        "bool": {
                            "must": [
                                {
                                    "ids" : {
                                        "values" : [userId]
                                    }
                                },
                                {
                                    "nested": {
                                        "path": "pubs",
                                        "query":{
                                        "bool": {
                                            "should": [
                                            {"multi_match" : {
                                                "query" : trigrams,
                                                "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams", "pubs.abstract_en.trigrams", "pubs.abstract_fr.trigrams"]
                                            }},
                                            trigrams_clean_en,
                                            trigrams_clean_fr,
                                            {"multi_match" : {
                                                "query" : unigrams,
                                                "fields" : ["pubs.title_en", "pubs.title_fr", "pubs.abstract_en", "pubs.abstract_fr", "pubs.keywords_en", "pubs.keywords_fr"]
                                            }}
                                        ]}}
                                        ,
                                        "inner_hits": {
                                            "size": 100,
                                            "explain": true
                                        }
                                    }
                                }

                            ]
                        }
                    }
                }
            });
        }
        getPublicationsByUserAndBigramsText(userId, bigrams, unigrams) {
            let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
            let bigrams_clean_en = [];
            let bigrams_clean_fr = [];
            let newtext = bigrams.match(re_bi);
            for(var i = 0; i < newtext.length; i++){
                bigrams_clean_en[i] = {
                        "match_phrase" : {
                        "pubs.keywords_en" : {
                            "query" : newtext[i],
                            "boost" : 2
                        }
                    }
                };
                bigrams_clean_fr[i] = {
                        "match_phrase" : {
                        "pubs.keywords_fr" : {
                            "query" : newtext[i],
                            "boost" : 2
                        }
                    }
                };
            }
            return this.client.search({
                index: this.globals.index_author,
                type: this.globals.doc_type_author,
                body: {
                    "size": 1,
                    "query": {
                        "bool": {
                            "must": [
                                {
                                    "ids" : {
                                        "values" : [userId]
                                    }
                                },
                                {
                                    "nested": {
                                        "path": "pubs",
                                        "query":{
                                        "bool": {
                                            "should": [
                                            {"multi_match" : {
                                                "query" : bigrams,
                                                "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams", "pubs.abstract_en.bigrams", "pubs.abstract_fr.bigrams"]
                                            }},
                                            bigrams_clean_fr,
                                            bigrams_clean_en,
                                            {"multi_match" : {
                                                "query" : unigrams,
                                                "fields" : ["pubs.title_en", "pubs.title_fr", "pubs.abstract_en", "pubs.abstract_fr", "pubs.keywords_en", "pubs.keywords_fr"]
                                            }}
                                        ]}}
                                        ,
                                        "inner_hits": {
                                            "size": 100,
                                            "explain": true
                                        }
                                    }
                                }

                            ]
                        }
                    }
                }
            });
        }
        getPublicationsByUserBigramsTrigrams(from, size, trigrams, bigrams) {
            let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
            let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
            let trigrams_clean_en = [];
            let trigrams_clean_fr = [];
            let bigrams_clean_en = [];
            let bigrams_clean_fr = [];
            let newtextbi = bigrams.match(re_bi);
            let newtexttri = trigrams.match(re_tri);
            for(var i = 0; i < newtextbi.length; i++){
                bigrams_clean_en[i] = {
                        "match_phrase" : {
                        "pubs.keywords_en" : {
                            "query" : newtextbi[i],
                            "boost" : 2
                        }
                    }
                };
                bigrams_clean_fr[i] = {
                        "match_phrase" : {
                        "pubs.keywords_fr" : {
                            "query" : newtextbi[i],
                            "boost" : 2
                        }
                    }
                };
            }
            for(var i = 0; i < newtexttri.length; i++){
                trigrams_clean_en[i] = {
                        "match_phrase" : {
                        "pubs.keywords_en" : {
                            "query" : newtexttri[i],
                            "boost" : 2
                        }
                    }
                };
                trigrams_clean_fr[i] = {
                        "match_phrase" : {
                        "pubs.keywords_fr" : {
                            "query" : newtexttri[i],
                            "boost" : 2
                        }
                    }
                };
            }
            return this.client.search({
                index: this.globals.index_author,
                type: this.globals.doc_type_author,
                body: {
                    "from" : from,
                    "size": size,
                    "query": {
                        "nested": {
                            "path": "pubs",
                            "query":{
                            "bool": {
                                "should": [
                                {"multi_match" : {
                                    "query" : trigrams,
                                    "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams", "pubs.abstract_en.trigrams", "pubs.abstract_fr.trigrams"]
                                }},
                                {"multi_match" : {
                                    "query" : bigrams,
                                    "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams", "pubs.abstract_en.bigrams", "pubs.abstract_fr.bigrams"]
                                }},
                                bigrams_clean_fr,
                                bigrams_clean_en,
                                trigrams_clean_fr,
                                trigrams_clean_en
                            ]}}
                            ,
                            "inner_hits": {
                                "size": 100,
                                "explain": true
                            }
                        }
                    }
                }
            });
        }
        getPublicationsByUserBigramsTrigramsText(from, size, trigrams, bigrams, text) {
            let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
            let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
            let trigrams_clean_en = [];
            let trigrams_clean_fr = [];
            let bigrams_clean_en = [];
            let bigrams_clean_fr = [];
            let newtextbi = bigrams.match(re_bi);
            let newtexttri = trigrams.match(re_tri);
            for(var i = 0; i < newtextbi.length; i++){
                bigrams_clean_en[i] = {
                        "match_phrase" : {
                        "pubs.keywords_en" : {
                            "query" : newtextbi[i],
                            "boost" : 2
                        }
                    }
                };
                bigrams_clean_fr[i] = {
                        "match_phrase" : {
                        "pubs.keywords_fr" : {
                            "query" : newtextbi[i],
                            "boost" : 2
                        }
                    }
                };
            }
            for(var i = 0; i < newtexttri.length; i++){
                trigrams_clean_en[i] = {
                        "match_phrase" : {
                        "pubs.keywords_en" : {
                            "query" : newtexttri[i],
                            "boost" : 2
                        }
                    }
                };
                trigrams_clean_fr[i] = {
                        "match_phrase" : {
                        "pubs.keywords_fr" : {
                            "query" : newtexttri[i],
                            "boost" : 2
                        }
                    }
                };
            }
            return this.client.search({
                index: this.globals.index_author,
                type: this.globals.doc_type_author,
                body: {
                    "from" : from,
                    "size": size,
                    "query": {
                        "nested": {
                            "path": "pubs",
                            "score_mode" : "sum",
                            "query":{
                            "bool": {
                                "should": [
                                {"multi_match" : {
                                    "query" : trigrams,
                                    "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams", "pubs.abstract_en.trigrams", "pubs.abstract_fr.trigrams"]
                                }},
                                {"multi_match" : {
                                    "query" : bigrams,
                                    "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams", "pubs.abstract_en.bigrams", "pubs.abstract_fr.bigrams"]
                                }},
                                bigrams_clean_fr,
                                bigrams_clean_en,
                                trigrams_clean_fr,
                                trigrams_clean_en,
                                {"multi_match" : {
                                    "query" : text,
                                    "fields" : ["pubs.title_en", "pubs.title_fr", "pubs.abstract_en", "pubs.abstract_fr", "pubs.keywords_en",
                                "pubs.keywords_fr"]
                                }}
                            ]}}
                            ,
                            "inner_hits": {
                                "size": 100,
                                "explain": true
                            }
                        }
                    }
                }
            });
        }
        getPublicationsByUserBigramsText(from, size, bigrams, text) {
            let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
            let bigrams_clean_en = [];
            let bigrams_clean_fr = [];
            let newtext = bigrams.match(re_bi);
            for(var i = 0; i < newtext.length; i++){
                bigrams_clean_en[i] = {
                        "match_phrase" : {
                        "pubs.keywords_en" : {
                            "query" : newtext[i],
                            "boost" : 2
                        }
                    }
                };
                bigrams_clean_fr[i] = {
                        "match_phrase" : {
                        "pubs.keywords_fr" : {
                            "query" : newtext[i],
                            "boost" : 2
                        }
                    }
                };
            }
            return this.client.search({
                index: this.globals.index_author,
                type: this.globals.doc_type_author,
                body: {
                    "from" : from,
                    "size": size,
                    "query": {
                        "nested": {
                            "path": "pubs",
                            "query":{
                            "bool": {
                                "should": [
                                {"multi_match" : {
                                    "query" : bigrams,
                                    "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams", "pubs.abstract_en.bigrams", "pubs.abstract_fr.bigrams"]
                                }},
                                bigrams_clean_fr,
                                bigrams_clean_en,
                                {"multi_match" : {
                                    "query" : text,
                                    "fields" : ["pubs.title_en", "pubs.title_fr", "pubs.abstract_en", "pubs.abstract_fr", "pubs.keywords_en" ,
                                "pubs.keywords_fr"]
                                }}
                            ]}}
                            ,
                            "inner_hits": {
                                "size": 100,
                                "explain": true
                            }
                        }
                    }
                }
            });
        }
        getPublicationsByUserTrigramsText(from, size, trigrams, text) {
            let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
            let trigrams_clean_en = [];
            let trigrams_clean_fr = [];
            let newtexttri = trigrams.match(re_tri);
            for(var i = 0; i < newtexttri.length; i++){
                trigrams_clean_en[i] = {
                        "match_phrase" : {
                        "pubs.keywords_en" : {
                            "query" : newtexttri[i],
                            "boost" : 2
                        }
                    }
                };
                trigrams_clean_fr[i] = {
                        "match_phrase" : {
                        "pubs.keywords_fr" : {
                            "query" : newtexttri[i],
                            "boost" : 2
                        }
                    }
                };
            }
            return this.client.search({
                index: this.globals.index_author,
                type: this.globals.doc_type_author,
                body: {
                    "size": size,
                    "from" : from,
                    "query": {
                        "nested": {
                            "path": "pubs",
                            "score_mode" : "sum",
                            "query":{
                            "bool": {
                                "should": [
                                {"multi_match" : {
                                    "query" : trigrams,
                                    "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams", "pubs.abstract_en.trigrams", "pubs.abstract_fr.trigrams"]
                                }},
                                {"multi_match" : {
                                    "query" : text,
                                    "fields" : ["pubs.title_en", "pubs.title_fr", "pubs.abstract_en", "pubs.abstract_fr", "pubs.keywords_en", "pubs.keywords_fr"]
                                }},
                                trigrams_clean_fr,
                                trigrams_clean_en
                            ]}}
                            ,
                            "inner_hits": {
                                "size": 100,
                                "explain": true
                            }
                        }
                    }
                }
            });
        }
        getPublicationsByUserAndKeywordTeam(userId, text) {
            return this.client.search({
                index: this.globals.index_author,
                type: this.globals.doc_type_author,
                body: {
                    "size": 1,
                    "query": {
                        "bool": {
                            "must": [
                                {
                                    "ids": {
                                        "values": [userId]
                                    }
                                },
                                {
                                    "nested": {
                                        "path": "pubs",
                                        "query":
                                        {
                                            "match": {
                                                "pubs.title_abstract": text
                                            }
                                        }
                                        ,
                                        "inner_hits": {
                                            "size": 100
                                        }
                                    }
                                }

                            ]
                        }
                    }
                }
            });
        }
        getPublicationsByUserAndKeywordId(userId, text) {
            return this.client.search({
                index: this.globals.index_author,
                type: this.globals.doc_type_author,
                body: {
                    "size": 1,
                    "query": {
                        "bool": {
                            "must": [
                                {
                                    "ids": {
                                        "values": [userId]
                                    }
                                },
                                {
                                    "nested": {
                                        "path": "pubs",
                                        "query":
                                        {
                                            "match": {
                                                "pubs.title_abstract": text
                                            }
                                        }
                                        ,
                                        "inner_hits": {
                                            "size": 100
                                        }
                                    }
                                }

                            ]
                        }
                    }
                }
            });
        }
        //Get significant Keywords of the application by comparing all publications with the affiliation publications
        getSignificantKeywordsAll(halId) {
            return this.client.search({
                index: this.globals.index_pub,
                type: this.globals.doc_type_pub,
                body: {
                    "_source": false,
                    "aggregations": {
                        "all_pub": {
                            "global": {},
                            "aggs": {
                                "pub_filter": {
                                    "filter": {
                                        "terms": {
                                            "halId": halId
                                        }
                                    },
                                    "aggs": {
                                        "most_sig_words": {
                                            "significant_terms": {
                                                "field": "title_abstract_sign",
                                                "size": 100,
                                                "min_doc_count": 4
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            })
        }
        //Get significant Keywords of the application by comparing all publications with the affiliation publications
        getSignificantKeywordsWithoutChronoAll(halId) {
            return this.client.search({
                index: this.globals.index_pub,
                type: this.globals.doc_type_pub,
                body: {
                    "_source": false,
                    "aggregations": {
                        "all_pub": {
                            "global": {},
                            "aggs": {
                                "pub_filter": {
                                    "filter": {
                                        "terms": {
                                            "halId": halId
                                        }
                                    },
                                    "aggs": {
                                        "most_sig_words": {
                                            "significant_terms": {
                                                "field": "title_abstract_sign",
                                                "size": 100
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            })
        }
        getUserExactMatch(name) {
            return this.client.search<any>({
                index: this.globals.index_author,
                type: this.globals.doc_type_author,
                body: {
                    "query": {
                        "match" : { "formatName" : name}

                    }
                }

            });
        }
        getUserMatch(name) {
            return this.client.search<any>({
                index: this.globals.index_author,
                type: this.globals.doc_type_author,
                body: {
                    "query": {
                        "bool" :{
                            "should" :[
                        {"match" : { "forename" : name}},
                        {"match" : { "surname" : name}},
                    ]}
                    }
                }

            });
        }
        textSuggestion(text) {
          let doc_type = this.globals.doc_type_pub;
            return this.client.suggest({
                index: this.globals.index_pub,
                body: {
                    doc_type : {
                        "text": text,
                        "completion": {
                            "size": 4,
                            "field": "doc.publication_suggest",
                            "suggest_mode": "popular",
                            "min_word_length": 2
                        }
                    }
                }
            });
        }
        suggestion(text) {
          return this.client.search<any>({
              index: this.globals.index_pub,
              type: this.globals.doc_type_pub,
              body: {
                  "_source" : "suggest",
                  "suggest" : {"text_suggest" :
                  {"prefix" : text, "completion" :
                  {"field" : "publication_suggest", "size" : 10}}
              }
            }
        });
        }

        suggestionPerson(text) {
          return this.client.search<any>({
              index: this.globals.index_author,
              type: this.globals.doc_type_author,
              body: {
                  "_source" : "suggest",
                  "suggest" : {"person_suggest" :
                  {"prefix" : text, "completion" :
                  {"field" : "person_suggest", "size" : 2}}
              }
            }
        });
        }
        getSuggestionWhenUserSearch(textSearch) : any {
            return this.client.search<any>({
                index: this.globals.index_pub,
                type : this.globals.doc_type_pub,
                body: {
                    "suggest": {
                        "mySuggest": {
                            "text": textSearch,
                            "term": {
                                "field": "title_abstract_sign",
                                "min_doc_freq": 10
                            }
                        }
                    }
                }
            })
        }
    getTextTopics(competencies) {
    return this.client.search({
        index: this.globals.index_pub,
        type: this.globals.doc_type_pub,
        body: {
            "size": 0,
            "query": {
                "multi_match": {
                    "query": competencies,
                    "fields": [
                        "keywords_*"
                    ],
                    "type": "most_fields"
                }
            },
            "aggs" : {
                "keywords_en" : {
                    "terms" : {
                        "size" : 5,
                        "field" : "keywords_en" }
                    },
                    "keywords_fr" : {
                       "terms" : {
                            "size" : 5,
                            "field" : "keywords_fr" }
                        }
                    }
                }
            });
        }
        //Get related keywords with word2vec
        getTextWords2vec(competencies) {
            return this.client.search({
                index: "word2vec",
                type: "word",
                body: {
                    "query": {
                        "match": {
                            "words.keyword" : competencies
                        }
                    }
                }
            })
        }
        // Create comment of user in elasticsearch
        setComment(userId, name, comment){
                return this.client.create({
                    index : 'comment',
                    type : 'indiv_comment',
                    body : {
                        indivId : userId,
                        name : name,
                        comment : comment
                    }
                }
            )
        }
        // register query from user for statistic
        setRequest(request){
                return this.client.create({
                    index : 'request_user',
                    type : 'request',
                    body : {
                    request : request
                    }
                }
            )
        }
}
