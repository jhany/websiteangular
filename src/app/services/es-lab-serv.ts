import { Injectable } from '@angular/core';
import * as elastic from "elasticsearch-browser";
import { ElasticService } from './es-serv';
import {Globals} from '../globals'

/**
* All the functionality to get or search affiliation or publication in Elasticsearch
*/
@Injectable()
export class SearchLabService{

    client : elastic.Client;
    private globals : Globals = new Globals();

    constructor(){
        this.client = new ElasticService().getClient();
    }

    //Get affiliation by id
    getAffiliationBastri(affiliationId) {
        return this.client.get<any>({
            index: this.globals.index_team,
            type: this.globals.doc_type_team,
            id: affiliationId
        });
    }
    // Get the teams similar to a given team
    getSimilarAffiliation(from, size, affiliationId) {
        return this.client.search<any>({
            index: this.globals.index_team,
            type: this.globals.doc_type_team,
            body: {
                "from": from,
                "size": size,
                "query": {
                    "nested": {
                        "path": "pubs",
                        "score_mode": "sum",
                        "query": {
                            "bool" :{
                                "should" : [
                                    {
                                        "more_like_this": {
                                            "fields": [
                                                "pubs.abstract_en", "pubs.abstract_fr"
                                            ],
                                            "like": [{
                                                "_id" : affiliationId
                                            }],
                                            "min_term_freq": 4,
                                            "max_query_terms" : 30,
                                            "min_word_length": 4
                                        }
                                    },
                                    {
                                        "more_like_this": {
                                            "fields": [
                                                "pubs.title_fr", "pubs.title_en"
                                            ],
                                            "like": [{
                                                "_id" : affiliationId
                                            }],
                                            "min_term_freq": 3,
                                            "min_word_length": 4,
                                            "max_query_terms" : 30,
                                            "boost" : 2
                                        }
                                    },
                                    {
                                        "more_like_this": {
                                            "fields": [
                                                "pubs.keywords_fr", "pubs.keywords_en"
                                            ],
                                            "like": [{
                                                "_id" : affiliationId
                                            }],
                                            "min_term_freq": 2,
                                            "max_query_terms" : 30,
                                            "min_word_length": 4,
                                            boost : 4
                                        }
                                    }
                                ]
                            }
                        }
                    }
                }
            }
        });
    }
    // Get the users similar to a given user
    getSimilarExplain(affiliationId, affiliationExplain) {
        return this.client.search<any>({
            index: this.globals.index_team,
            type: this.globals.doc_type_team,
            body: {
                "_source" : false,
                "size": 1,
                "query": {
                    bool: {
                        must: [
                            {
                                match: {
                                    id : affiliationExplain
                                }
                            },
                            {

                                "nested": {
                                    "path": "pubs",
                                    "query": {
                                        "bool": {
                                            "should": [
                                                {
                                                    "more_like_this": {
                                                        "fields": [
                                                            "pubs.abstract_en", "pubs.abstract_fr"
                                                        ],
                                                        "like": [{
                                                            "_id" : affiliationId
                                                        }],
                                                        "min_term_freq": 4,
                                                        "max_query_terms" : 30,
                                                        "min_word_length": 4
                                                    }
                                                },
                                                {
                                                    "more_like_this": {
                                                        "fields": [
                                                            "pubs.title_fr", "pubs.title_en"
                                                        ],
                                                        "like": [{
                                                            "_id" : affiliationId
                                                        }],
                                                        "min_term_freq": 3,
                                                        "min_word_length": 4,
                                                        "max_query_terms" : 30
                                                    }
                                                },
                                                {
                                                    "more_like_this": {
                                                        "fields": [
                                                            "pubs.keywords_*"
                                                        ],
                                                        "like": [{
                                                            "_id" : affiliationId
                                                        }],
                                                        "min_term_freq": 2,
                                                        "max_query_terms" : 30,
                                                        "min_word_length": 4
                                                    }
                                                }
                                            ]
                                        }
                                    },
                                    "inner_hits": {
                                        "explain": true
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        });
    }
    getAffiliationsExplainBigram(id, text){
      return this.client.search<any>({
        index: this.globals.index_team,
        type: this.globals.doc_type_team,
        body:{
        "query": {
          "bool": {
            "must": [
              {
                "match": {
                  "id": id
                }
              },
              {
                "nested": {
                  "path": "pubs",
                  "query": {
                    "bool": {
                      "should": [
                        {
                          "more_like_this": {
                            "fields": [
                              "pubs.abstract_en",
                              "pubs.abstract_fr",
                              "pubs.title_fr",
                              "pubs.title_en"
                            ],
                            "like": text,
                            "min_term_freq": 1,
                            "max_query_terms": 60,
                            "min_word_length": 4,
                            "minimum_should_match" : "15%"
                          }
                        }
                      ]
                    }
                  },
                  "inner_hits": {
                    "explain": true
                  }
                }
              }
            ]
          }
        }
      }
      })
    }
    //Get affiliation by competencies
    getAffiliationsExist(competencies) {
                return this.client.search<any>({
                    index: this.globals.index_team,
                    type: this.globals.doc_type_team,
                    body: {
                        "query": {
                                    "match" : {"acronym" : {"query" : competencies}}
                                }
                            }
                        })
                    }
    //Get affiliation by competencies
    /**
        "bool" : {
            "should" :[
                {"match" : {"acronym" : {"query" : competencies}}},
    **/
    getAffiliationsByCompetenciesExist(from, size, competencies) {
                return this.client.search<any>({
                    index: this.globals.index_team,
                    type: this.globals.doc_type_team,
                    body: {
                        "from": from,
                        "size": size,
                        "query": {
                            "bool" : {
                                "must_not" :{
                                    "match" : {
                                        "acronym" : {"query" : competencies}
                                    }
                                },
                        "should": {
                            "nested": {
                                "path": "pubs",
                                "score_mode" : "sum",
                                "query": {
                                    "function_score" : {
                                        "script_score" : {
                                            "script" : {
                                                "lang": "painless",
                                                "inline": "_score * _score * _score * _score"
                                            }
                                        },
                                        "boost_mode":"multiply",
                                        "query" : {
                                            "bool": {
                                                "should": [
                                                    {
                                                        "multi_match": {
                                                            "fields": [
                                                                "pubs.abstract_en",
                                                                "pubs.abstract_fr"
                                                            ],
                                                            "query": competencies,
                                                            "boost": 2
                                                        }
                                                    },
                                                    {
                                                        "multi_match": {
                                                            "fields": [
                                                                "pubs.title_en",
                                                                "pubs.title_fr"
                                                            ],
                                                            "query" : competencies,
                                                            "boost": 4
                                                        }
                                                    },
                                                    {
                                                        "multi_match": {
                                                            "query" : competencies,
                                                            "fields" : ["pubs.keywords_*"],
                                                            "type":       "most_fields",
                                                            "boost": 10
                                                        }
                                                    }
                                                ]
                                            }
                                        }
                                    }
                                }
                            }}
                        }}
                        }
                }
            )}
    //Get affiliation by competencies
    getAffiliationsByCompetencies(from, size, competencies) {
                return this.client.search<any>({
                    index: this.globals.index_team,
                    type: this.globals.doc_type_team,
                    body: {
                        "from": from,
                        "size": size,
                        "query": {
                            "nested": {
                                "path": "pubs",
                                "score_mode" : "sum",
                                "query": {
                                    "function_score" : {
                                        "script_score" : {
                                            "script" : {
                                                "lang": "painless",
                                                "inline": "_score * _score * _score * _score"
                                            }
                                        },
                                        "boost_mode":"multiply",
                                        "query" : {
                                            "bool": {
                                                "should": [
                                                    {
                                                        "multi_match": {
                                                            "fields": [
                                                                "pubs.abstract_en",
                                                                "pubs.abstract_fr"
                                                            ],
                                                            "query": competencies,
                                                            "boost": 2
                                                        }
                                                    },
                                                    {
                                                        "multi_match": {
                                                            "fields": [
                                                                "pubs.title_en",
                                                                "pubs.title_fr"
                                                            ],
                                                            "query" : competencies,
                                                            "boost": 4
                                                        }
                                                    },
                                                    {
                                                        "multi_match": {
                                                            "query" : competencies,
                                                            "fields" : ["pubs.keywords_*"],
                                                            "type":       "most_fields",
                                                            "boost": 10
                                                        }
                                                    },
                                                    {
                                                        "multi_match": {
                                                            "query" : competencies,
                                                            "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams"],
                                                            "type":       "most_fields",
                                                            "boost": 150
                                                        }
                                                    },
                                                    {
                                                        "multi_match": {
                                                            "query" : competencies,
                                                            "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams"],
                                                            "type":       "most_fields",
                                                            "boost": 200
                                                        }
                                                    }
                                                ]
                                            }
                                        }
                                    }
                                }
                        }
                        }
                    }
                }
            )}
    getSignificantAllDomainKeywords(domain, affiliationId){
        return this.client.search({
            index: this.globals.index_pub,
            type: this.globals.doc_type_pub,
            body: {
                "_source": false,
                "query": {
                    "nested": {
                        "path": "hal_domains",
                        "query": {
                            "term": {
                                "hal_domains.title": domain
                            }
                        }
                    }
                },
                "aggregations": {
                    "affiliations": {
                        "nested": {
                            "path": "affiliations"
                        },
                        "aggs": {
                            "affiliationfilter": {
                                "filter": {
                                    "term": {
                                        "affiliations.id": affiliationId
                                    }
                                },
                                "aggs": {
                                    "got_back": {
                                        "reverse_nested": {},
                                        "aggs": {
                                            "most_sig_en": {
                                                "significant_terms": {
                                                    "field": "title_abstract_sign",
                                                    "size": 100
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        })
    }
    //Get significant Keywords of the application by comparing all publications with the affiliation publications
    getSignificantKeywordsAffAll(affiliationId) {
        return this.client.search({
            index: this.globals.index_pub,
            type: this.globals.doc_type_pub,
            body: {
                "_source": false,
                "query": {
                    "match_all": {}
                },
                "aggregations": {
                    "affiliations": {
                        "nested": {
                            "path": "affiliations"
                        },
                        "aggs": {
                            "affiliationfilter": {
                                "filter": {
                                    "term": {
                                        "affiliations.id": affiliationId
                                    }
                                },
                                "aggs": {
                                    "got_back": {
                                        "reverse_nested": {},
                                        "aggs": {
                                            "most_sig_words": {
                                                "significant_terms": {
                                                    "field": "title_abstract_sign",
                                                    "size": 100
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        })
    }
    //users in a affiliation
    getAffiliationUsers(from, size, id){
        return this.client.search({
            index: this.globals.index_author,
            type: this.globals.doc_type_author,
            body: {
                "from": from,
                "size" : size,
                "query": {
                    "nested": {
           "path": "history",
           "query": {
               "match": {
                  "history.structure": id
               }
           }
        }
                }
            }
        });
    }
    // Get significant keywords for a domain compare to all publication of this domain
    getSignificantKeywordsAffAllSubDomain(affiliationId, subDomain) {
        return this.client.search({
            index: this.globals.index_pub,
            type: this.globals.doc_type_pub,
            body: {
                "_source": false,
                "query": {
                    "nested": {
                        "path": "hal_domains",
                        "query": {
                            "terms": {
                                "hal_domains.title": subDomain
                            }
                        }
                    }
                },
                "aggregations": {
                    "affiliations": {
                        "nested": {
                            "path": "affiliations"
                        },
                        "aggs": {
                            "affiliationfilter": {
                                "filter": {
                                    "term": {
                                        "affiliations.id": affiliationId
                                    }
                                },
                                "aggs": {
                                    "got_back": {
                                        "reverse_nested": {},
                                        "aggs": {
                                            "most_sig_en": {
                                                "significant_terms": {
                                                    "field": "title_abstract_sign",
                                                    "size": 100
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        })
    }
    //Get publication with significant keywords use when user click on word cloud
    getPublicationBySignificantKeywords(from, size, affiliationId, significantKeywords){
        return this.client.search({
            index: this.globals.index_team,
            type: this.globals.doc_type_team,
            body: {
                "from": from,
                "size": size,
                "query": {
                    "bool": {
                        "must":[
                            {"nested":{
                                "path":"pubs",
                                "query":{
                                    "match":{"pubs.title_abstract": significantKeywords}},
                                    "inner_hits":{
                                        size : 100
                                    }}},
                                    {"match":{"id":affiliationId}}]
                                }
                            }
                        }
                    })
                }
        //Get publication with significant keywords use when user click on word cloud
        getPublicationBySignificantBigrams(from, size, affiliationId, bigrams){
        let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let bigrams_clean_en = [];
        let bigrams_clean_fr = [];
        let newtext = bigrams.match(re_bi);
        for(var i = 0; i < newtext.length; i++){
            bigrams_clean_en[i] = {
                    "match_phrase" : {
                    "pubs.keywords_en" : {
                        "query" : newtext[i],
                        "boost" : 2
                    }
                }
            };
            bigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "pubs.keywords_fr" : {
                        "query" : newtext[i],
                        "boost" : 2
                    }
                }
            };
        }
            return this.client.search({
                index: this.globals.index_team,
                type: this.globals.doc_type_team,
                body: {
                    "from": from,
                    "size": size,
                    "query": {
                        "bool": {
                            "must":[
                                {"nested":{
                                    "path":"pubs",
                                    "query":{
                                        "bool": {
                                            "should": [
                                            {"multi_match" : {
                                                "query" : bigrams,
                                                "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams", "pubs.abstract_en.bigrams", "pubs.abstract_fr.bigrams"]
                                            }},
                                            bigrams_clean_fr,
                                            bigrams_clean_en
                                        ]}}
                                        ,
                                        "inner_hits":{
                                            size : 100
                                        }}},
                                        {"match":{"id":affiliationId}}]
                                    }
                                }
                            }
                        })
                    }
        //Get publication with significant keywords use when user click on word cloud
        getPublicationBySignificantTrigrams(from, size, affiliationId, trigrams){
        let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let trigrams_clean_en = [];
        let trigrams_clean_fr = [];
        let newtexttri = trigrams.match(re_tri);
        for(var i = 0; i < newtexttri.length; i++){
            trigrams_clean_en[i] = {
                    "match_phrase" : {
                    "pubs.keywords_en" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
            trigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "pubs.keywords_fr" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
        }
            return this.client.search({
                index: this.globals.index_team,
                type: this.globals.doc_type_team,
                body: {
                    "from": from,
                    "size": size,
                    "query": {
                        "bool": {
                            "must":[
                                {"nested":{
                                    "path":"pubs",
                                    "query":{
                                        "bool": {
                                            "should": [
                                            {"multi_match" : {
                                                "query" : trigrams,
                                                "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams", "pubs.abstract_en.trigrams", "pubs.abstract_fr.trigrams"]
                                            }},
                                            trigrams_clean_fr,
                                            trigrams_clean_en
                                        ]}}
                                        ,
                                        "inner_hits":{
                                            size : 100
                                        }}},
                                        {"match":{"id":affiliationId}}]
                                    }
                                }
                            }
                        })
                    }
            //Get publication with significant keywords use when user click on word cloud
            getPublicationBySignificantBigramsTrigrams(from, size, affiliationId, trigrams, bigrams){
            let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
            let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
            let trigrams_clean_en = [];
            let trigrams_clean_fr = [];
            let bigrams_clean_en = [];
            let bigrams_clean_fr = [];
            let newtextbi = bigrams.match(re_bi);
            let newtexttri = trigrams.match(re_tri);
            for(var i = 0; i < newtextbi.length; i++){
                bigrams_clean_en[i] = {
                        "match_phrase" : {
                        "pubs.keywords_en" : {
                            "query" : newtextbi[i],
                            "boost" : 2
                        }
                    }
                };
                bigrams_clean_fr[i] = {
                        "match_phrase" : {
                        "pubs.keywords_fr" : {
                            "query" : newtextbi[i],
                            "boost" : 2
                        }
                    }
                };
            }
            for(var i = 0; i < newtexttri.length; i++){
                trigrams_clean_en[i] = {
                        "match_phrase" : {
                        "pubs.keywords_en" : {
                            "query" : newtexttri[i],
                            "boost" : 2
                        }
                    }
                };
                trigrams_clean_fr[i] = {
                        "match_phrase" : {
                        "pubs.keywords_fr" : {
                            "query" : newtexttri[i],
                            "boost" : 2
                        }
                    }
                };
            }
                return this.client.search({
                    index: this.globals.index_team,
                    type: this.globals.doc_type_team,
                    body: {
                        "from": from,
                        "size": size,
                        "query": {
                            "bool": {
                                "must":[
                                    {"nested":{
                                        "path":"pubs",
                                        "query":{
                                            "bool": {
                                                "should": [
                                                {"multi_match" : {
                                                    "query" : trigrams,
                                                    "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams", "pubs.abstract_en.trigrams", "pubs.abstract_fr.trigrams"]
                                                }},
                                                trigrams_clean_en,
                                                trigrams_clean_fr,
                                                {"multi_match" : {
                                                    "query" : bigrams,
                                                    "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams", "pubs.abstract_en.bigrams", "pubs.abstract_fr.bigrams"]
                                                }},
                                                bigrams_clean_en,
                                                bigrams_clean_fr
                                            ]}}
                                            ,
                                            "inner_hits":{
                                                size : 100
                                            }}},
                                            {"match":{"id":affiliationId}}]
                                        }
                                    }
                                }
                            })
                        }
                //Get publication with significant keywords use when user click on word cloud
                getPublicationBySignificantBigramsTrigramsText(from, size, affiliationId, trigrams, bigrams, unigrams){
                let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
                let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
                let trigrams_clean_en = [];
                let trigrams_clean_fr = [];
                let bigrams_clean_en = [];
                let bigrams_clean_fr = [];
                let newtextbi = bigrams.match(re_bi);
                let newtexttri = trigrams.match(re_tri);
                for(var i = 0; i < newtextbi.length; i++){
                    bigrams_clean_en[i] = {
                            "match_phrase" : {
                            "pubs.keywords_en" : {
                                "query" : newtextbi[i],
                                "boost" : 2
                            }
                        }
                    };
                    bigrams_clean_fr[i] = {
                            "match_phrase" : {
                            "pubs.keywords_fr" : {
                                "query" : newtextbi[i],
                                "boost" : 2
                            }
                        }
                    };
                }
                for(var i = 0; i < newtexttri.length; i++){
                    trigrams_clean_en[i] = {
                            "match_phrase" : {
                            "pubs.keywords_en" : {
                                "query" : newtexttri[i],
                                "boost" : 2
                            }
                        }
                    };
                    trigrams_clean_fr[i] = {
                            "match_phrase" : {
                            "pubs.keywords_fr" : {
                                "query" : newtexttri[i],
                                "boost" : 2
                            }
                        }
                    };
                }
                    return this.client.search({
                        index: this.globals.index_team,
                        type: this.globals.doc_type_team,
                        body: {
                            "from": from,
                            "size": size,
                            "query": {
                                "bool": {
                                    "must":[
                                        {"nested":{
                                            "path":"pubs",
                                            "query":{
                                                "bool": {
                                                    "should": [
                                                    {"multi_match" : {
                                                        "query" : trigrams,
                                                        "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams", "pubs.abstract_en.trigrams", "pubs.abstract_fr.trigrams"]
                                                    }},
                                                    trigrams_clean_en,
                                                    trigrams_clean_fr,
                                                    {"multi_match" : {
                                                        "query" : bigrams,
                                                        "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams", "pubs.abstract_en.bigrams", "pubs.abstract_fr.bigrams"]
                                                    }},
                                                    bigrams_clean_fr,
                                                    bigrams_clean_en,
                                                    {"multi_match" : {
                                                        "query" : unigrams,
                                                        "fields" : ["pubs.title_en", "pubs.title_fr", "pubs.abstract_en", "pubs.abstract_fr", "pubs.keywords_en", "pubs.keywords_fr"]
                                                    }}
                                                ]}}
                                                ,
                                                "inner_hits":{
                                                    size : 100
                                                }}},
                                                {"match":{"id":affiliationId}}]
                                            }
                                        }
                                    }
                                })
                            }
            //Get publication with significant keywords use when user click on word cloud
            getPublicationBySignificantTrigramsText(from, size, affiliationId, trigrams, unigrams){
            let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
            let trigrams_clean_en = [];
            let trigrams_clean_fr = [];
            let newtexttri = trigrams.match(re_tri);
            for(var i = 0; i < newtexttri.length; i++){
                trigrams_clean_en[i] = {
                        "match_phrase" : {
                        "pubs.keywords_en" : {
                            "query" : newtexttri[i],
                            "boost" : 2
                        }
                    }
                };
                trigrams_clean_fr[i] = {
                        "match_phrase" : {
                        "pubs.keywords_fr" : {
                            "query" : newtexttri[i],
                            "boost" : 2
                        }
                    }
                };
            }
                return this.client.search({
                    index: this.globals.index_team,
                    type: this.globals.doc_type_team,
                    body: {
                        "from": from,
                        "size": size,
                        "query": {
                            "bool": {
                                "must":[
                                    {"nested":{
                                        "path":"pubs",
                                        "query":{
                                            "bool": {
                                                "should": [
                                                {"multi_match" : {
                                                    "query" : trigrams,
                                                    "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams", "pubs.abstract_en.trigrams", "pubs.abstract_fr.trigrams"]
                                                }},
                                                trigrams_clean_fr,
                                                trigrams_clean_en,
                                                {"multi_match" : {
                                                    "query" : unigrams,
                                                    "fields" : ["pubs.title_en", "pubs.title_fr", "pubs.abstract_en", "pubs.abstract_fr", "pubs.keywords_en", "pubs.keywords_fr"]
                                                }}
                                            ]}}
                                            ,
                                            "inner_hits":{
                                                size : 100
                                            }}},
                                            {"match":{"id":affiliationId}}]
                                        }
                                    }
                                }
                            })
                    }
            //Get publication with significant keywords use when user click on word cloud
            getPublicationBySignificantTBigramsText(from, size, affiliationId, bigrams, unigrams){
            let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
            let bigrams_clean_en = [];
            let bigrams_clean_fr = [];
            let newtext = bigrams.match(re_bi);
            for(var i = 0; i < newtext.length; i++){
                bigrams_clean_en[i] = {
                        "match_phrase" : {
                        "pubs.keywords_en" : {
                            "query" : newtext[i],
                            "boost" : 2
                        }
                    }
                };
                bigrams_clean_fr[i] = {
                        "match_phrase" : {
                        "pubs.keywords_fr" : {
                            "query" : newtext[i],
                            "boost" : 2
                        }
                    }
                };
            }
                return this.client.search({
                    index: this.globals.index_team,
                    type: this.globals.doc_type_team,
                    body: {
                        "from": from,
                        "size": size,
                        "query": {
                            "bool": {
                                "must":[
                                    {"nested":{
                                        "path":"pubs",
                                        "query":{
                                            "bool": {
                                                "should": [
                                                {"multi_match" : {
                                                    "query" : bigrams,
                                                    "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams", "pubs.abstract_en.bigrams", "pubs.abstract_fr.bigrams"]
                                                }},
                                                bigrams_clean_fr,
                                                bigrams_clean_en,
                                                {"multi_match" : {
                                                    "query" : unigrams,
                                                    "fields" : ["pubs.title_en", "pubs.title_fr", "pubs.abstract_en", "pubs.abstract_fr", "pubs.keywords_en", "pubs.keywords_fr"]
                                                }}
                                            ]}}
                                            ,
                                            "inner_hits":{
                                                size : 100
                                            }}},
                                            {"match":{"id":affiliationId}}]
                                        }
                                    }
                                }
                            })
                        }
    //Get the number of publication for a user research Keywords on a affiliations
    getPublicationByAffiliationKeywords(from, size, affiliationId, significantKeywords){
        return this.client.search({
            index: this.globals.index_pub,
            type: this.globals.doc_type_pub,
            body: {
                "from": from,
                "size": size,
                "query": {
                    "bool": {
                        "must": [
                            {
                                "match": {
                                    "title_abstract": significantKeywords
                                }
                            },
                            {
                                "nested": {
                                    "path": "affiliations",
                                    "query": {
                                        "match": {
                                            "affiliations.id": affiliationId
                                        }
                                    },
                                    "inner_hits" : {}
                                }
                            }
                        ]
                    }
                }
            }
        })
    }
    //Get the number of publication for a user research Keywords on a affiliations
    getPublicationByAffiliationBigrams(from, size, affiliationId, significantKeywords){
        return this.client.search({
            index: this.globals.index_pub,
            type: this.globals.doc_type_pub,
            body: {
                "from": from,
                "size": size,
                "query": {
                    "bool": {
                        "must": [
                            {
                                "multi_match" : {
                                    "query" : significantKeywords,
                                    "fields" : ["keywords_fr", "keywords_en", "title_en.bigrams", "title_fr.bigrams", "abstract_en.bigrams", "abstract_fr.bigrams"]
                                }
                            },
                            {
                                "nested": {
                                    "path": "affiliations",
                                    "query": {
                                        "match": {
                                            "affiliations.id": affiliationId
                                        }
                                    },
                                    "inner_hits" : {}
                                }
                            }
                        ]
                    }
                }
            }
        })
    }
    getAffiliationsWithBigram(from, size, bigrams){
        let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let bigrams_clean_en = [];
        let bigrams_clean_fr = [];
        let newtext = bigrams.match(re_bi);
        for(var i = 0; i < newtext.length; i++){
            bigrams_clean_en[i] = {
                    "match_phrase" : {
                    "pubs.keywords_en" : {
                        "query" : newtext[i],
                        "boost" : 2
                    }
                }
            };
            bigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "pubs.keywords_fr" : {
                        "query" : newtext[i],
                        "boost" : 2
                    }
                }
            };
        }
        return this.client.search({
            index: this.globals.index_team,
            type: this.globals.doc_type_team,
            body: {
                "from": from,
                "size": size,
                "query": {
                    "nested": {
                        "path": "pubs",
                        "score_mode" : "sum",
                        "query": {
                                    "bool": {
                                        "should": [
                                            {
                                                "multi_match" : {
                                                    "query" : bigrams,
                                                    "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams"],
                                                    "boost" : 2
                                                }
                                            },
                                            {
                                                "multi_match" : {
                                                    "query" : bigrams,
                                                    "fields" : ["pubs.abstract_en.bigrams", "pubs.abstract_fr.bigrams"]
                                                }
                                            },
                                            bigrams_clean_en,
                                            bigrams_clean_fr
                                        ]
                                    }
                            }
                    }
                }
            }
        }
    )}
    getAffiliationsWithTrigram(from, size, trigrams){
        let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let trigrams_clean_en = [];
        let trigrams_clean_fr = [];
        let newtexttri = trigrams.match(re_tri);
        for(var i = 0; i < newtexttri.length; i++){
            trigrams_clean_en[i] = {
                    "match_phrase" : {
                    "pubs.keywords_en" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
            trigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "pubs.keywords_fr" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
        }
        return this.client.search({
            index: this.globals.index_team,
            type: this.globals.doc_type_team,
            body: {
                "from": from,
                "size": size,
                "query": {
                    "nested": {
                        "path": "pubs",
                        "score_mode" : "sum",
                                "query" : {
                                    "bool": {
                                        "should": [
                                            {
                                                "multi_match" : {
                                                    "query" : trigrams,
                                                    "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams"],
                                                    "boost" : 2
                                                }
                                            },
                                            {
                                                "multi_match" : {
                                                    "query" : trigrams,
                                                    "fields" : ["pubs.abstract_en.trigrams", "pubs.abstract_fr.trigrams"]
                                                }
                                            },
                                            trigrams_clean_en,
                                            trigrams_clean_fr
                                        ]
                                    }
                                }
                    }
                }
            }
        }
    )}
    getPublicationsByAffBigramsTrigrams(from, size, trigrams, bigrams) {
        let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let trigrams_clean_en = [];
        let trigrams_clean_fr = [];
        let bigrams_clean_en = [];
        let bigrams_clean_fr = [];
        let newtextbi = bigrams.match(re_bi);
        let newtexttri = trigrams.match(re_tri);
        for(var i = 0; i < newtextbi.length; i++){
            bigrams_clean_en[i] = {
                    "match_phrase" : {
                    "pubs.keywords_en" : {
                        "query" : newtextbi[i],
                        "boost" : 2
                    }
                }
            };
            bigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "pubs.keywords_fr" : {
                        "query" : newtextbi[i],
                        "boost" : 2
                    }
                }
            };
        }
        for(var i = 0; i < newtexttri.length; i++){
            trigrams_clean_en[i] = {
                    "match_phrase" : {
                    "pubs.keywords_en" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
            trigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "pubs.keywords_fr" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
        }
        return this.client.search({
            index: this.globals.index_team,
            type: this.globals.doc_type_team,
            body: {
                "from": from,
                "size": size,
                "query": {
                    "nested": {
                        "path": "pubs",
                        "query":{
                        "bool": {
                            "should": [
                            {"multi_match" : {
                                "query" : trigrams,
                                "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams", "pubs.abstract_en.trigrams", "pubs.abstract_fr.trigrams"]
                            }},
                            {"multi_match" : {
                                "query" : bigrams,
                                "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams", "pubs.abstract_en.bigrams", "pubs.abstract_fr.bigrams"]
                            }},
                            bigrams_clean_fr,
                            bigrams_clean_en,
                            trigrams_clean_fr,
                            trigrams_clean_en
                        ]}}
                    }
                }
            }
        });
    }
    getPublicationsByAffBigramsTrigramsText(from, size, trigrams, bigrams, text) {
        let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let trigrams_clean_en = [];
        let trigrams_clean_fr = [];
        let bigrams_clean_en = [];
        let bigrams_clean_fr = [];
        let newtextbi = bigrams.match(re_bi);
        let newtexttri = trigrams.match(re_tri);
        for(var i = 0; i < newtextbi.length; i++){
            bigrams_clean_en[i] = {
                    "match_phrase" : {
                    "pubs.keywords_en" : {
                        "query" : newtextbi[i],
                        "boost" : 2
                    }
                }
            };
            bigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "pubs.keywords_fr" : {
                        "query" : newtextbi[i],
                        "boost" : 2
                    }
                }
            };
        }
        for(var i = 0; i < newtexttri.length; i++){
            trigrams_clean_en[i] = {
                    "match_phrase" : {
                    "pubs.keywords_en" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
            trigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "pubs.keywords_fr" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
        }
        return this.client.search({
            index: this.globals.index_team,
            type: this.globals.doc_type_team,
            body: {
                "from": from,
                "size": size,
                "query": {
                    "nested": {
                        "path": "pubs",
                        "score_mode" : "sum",
                        "query":{
                        "bool": {
                            "should": [
                            {"multi_match" : {
                                "query" : trigrams,
                                "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams", "pubs.abstract_en.trigrams", "pubs.abstract_fr.trigrams"]
                            }},
                            {"multi_match" : {
                                "query" : bigrams,
                                "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams", "pubs.abstract_en.bigrams", "pubs.abstract_fr.bigrams"]
                            }},
                            bigrams_clean_fr,
                            bigrams_clean_en,
                            trigrams_clean_fr,
                            trigrams_clean_en,
                            {"multi_match" : {
                                "query" : text,
                                "fields" : ["pubs.title_en", "pubs.title_fr", "pubs.abstract_en", "pubs.abstract_fr", "pubs.keywords_en", "pubs.keywords_fr"]
                            }}
                        ]}}
                    }
                }
            }
        });
    }
    getPublicationsByAffBigramsText(from, size, bigrams, text) {
        let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let bigrams_clean_en = [];
        let bigrams_clean_fr = [];
        let newtext = bigrams.match(re_bi);
        for(var i = 0; i < newtext.length; i++){
            bigrams_clean_en[i] = {
                    "match_phrase" : {
                    "pubs.keywords_en" : {
                        "query" : newtext[i],
                        "boost" : 2
                    }
                }
            };
            bigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "pubs.keywords_fr" : {
                        "query" : newtext[i],
                        "boost" : 2
                    }
                }
            };
        }
        return this.client.search({
            index: this.globals.index_team,
            type: this.globals.doc_type_team,
            body: {
                "from": from,
                "size": size,
                "query": {
                    "nested": {
                        "path": "pubs",
                        "query":{
                        "bool": {
                            "should": [
                            {"multi_match" : {
                                "query" : bigrams,
                                "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams", "pubs.abstract_en.bigrams", "pubs.abstract_fr.bigrams"]
                            }},
                            bigrams_clean_fr,
                            bigrams_clean_en,
                            {"multi_match" : {
                                "query" : text,
                                "fields" : ["pubs.title_en", "pubs.title_fr", "pubs.abstract_en", "pubs.abstract_fr", "pubs.keywords_en", "pubs.keywords_fr"]
                            }}
                        ]}}
                    }
                }
            }
        });
    }
    getPublicationsByAffTrigramsText(from, size, trigrams, text) {
        let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let trigrams_clean_en = [];
        let trigrams_clean_fr = [];
        let newtexttri = trigrams.match(re_tri);
        for(var i = 0; i < newtexttri.length; i++){
            trigrams_clean_en[i] = {
                    "match_phrase" : {
                    "pubs.keywords_en" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
            trigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "pubs.keywords_fr" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
        }
        return this.client.search({
            index: this.globals.index_team,
            type: this.globals.doc_type_team,
            body: {
                "from": from,
                "size": size,
                "query": {
                    "nested": {
                        "path": "pubs",
                        "score_mode" : "sum",
                        "query":{
                        "bool": {
                            "should": [
                            {"multi_match" : {
                                "query" : trigrams,
                                "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams", "pubs.abstract_en.trigrams", "pubs.abstract_fr.trigrams"]
                            }},
                            {"multi_match" : {
                                "query" : text,
                                "fields" : ["pubs.title_en", "pubs.title_fr", "pubs.abstract_en", "pubs.abstract_fr", "pubs.keywords_en","pubs.keywords_fr"]
                            }},
                            trigrams_clean_en,
                            trigrams_clean_fr
                        ]}}
                    }
                }
            }
        });
    }
    getAffiliationsWithBigramTrigramsExplain(affiliationId, bigrams, trigrams){
        let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let trigrams_clean_en = [];
        let trigrams_clean_fr = [];
        let bigrams_clean_en = [];
        let bigrams_clean_fr = [];
        let newtextbi = bigrams.match(re_bi);
        let newtexttri = trigrams.match(re_tri);
        for(var i = 0; i < newtextbi.length; i++){
            bigrams_clean_en[i] = {
                    "match_phrase" : {
                    "pubs.keywords_en" : {
                        "query" : newtextbi[i],
                        "boost" : 2
                    }
                }
            };
            bigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "pubs.keywords_fr" : {
                        "query" : newtextbi[i],
                        "boost" : 2
                    }
                }
            };
        }
        for(var i = 0; i < newtexttri.length; i++){
            trigrams_clean_en[i] = {
                    "match_phrase" : {
                    "pubs.keywords_en" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
            trigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "pubs.keywords_fr" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
        }
        return this.client.search({
            index: this.globals.index_team,
            type: this.globals.doc_type_team,
            body: {
                "query": {
                    "bool": {
                        "must": [
                            {
                                "term": {
                                    "id": affiliationId
                                }
                            }, {
                                "nested": {
                                    "path": "pubs",
                                    "score_mode" : "sum",
                                    "query": {
                                                "bool": {
                                                    "should": [
                                                        {
                                                            "multi_match" : {
                                                                "query" : bigrams,
                                                                "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams"],
                                                                "boost" : 2
                                                            }
                                                        },
                                                        {
                                                            "multi_match" : {
                                                                "query" : bigrams,
                                                                "fields" : ["pubs.abstract_en.bigrams", "pubs.abstract_fr.bigrams"]
                                                            }
                                                        },
                                                        bigrams_clean_fr,
                                                        bigrams_clean_en,
                                                        {
                                                            "multi_match" : {
                                                                "query" : trigrams,
                                                                "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams"],
                                                                "boost" : 2
                                                            }
                                                        },
                                                        {
                                                            "multi_match" : {
                                                                "query" : trigrams,
                                                                "fields" : ["pubs.abstract_en.trigrams", "pubs.abstract_fr.trigrams"]
                                                            }
                                                        },
                                                        trigrams_clean_fr,
                                                        trigrams_clean_en
                                                    ]
                                                }
                                        },
                                    "inner_hits": {
                                        "explain": true
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        }
    )}
    getAffiliationsWithBigramTrigramsTextExplain(affiliationId, bigrams, trigrams, text){
        let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let trigrams_clean_en = [];
        let trigrams_clean_fr = [];
        let bigrams_clean_en = [];
        let bigrams_clean_fr = [];
        let newtextbi = bigrams.match(re_bi);
        let newtexttri = trigrams.match(re_tri);
        for(var i = 0; i < newtextbi.length; i++){
            bigrams_clean_en[i] = {
                    "match_phrase" : {
                    "pubs.keywords_en" : {
                        "query" : newtextbi[i],
                        "boost" : 2
                    }
                }
            };
            bigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "pubs.keywords_fr" : {
                        "query" : newtextbi[i],
                        "boost" : 2
                    }
                }
            };
        }
        for(var i = 0; i < newtexttri.length; i++){
            trigrams_clean_en[i] = {
                    "match_phrase" : {
                    "pubs.keywords_en" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
            trigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "pubs.keywords_fr" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
        }
        return this.client.search({
            index: this.globals.index_team,
            type: this.globals.doc_type_team,
            body: {
                "query": {
                    "bool": {
                        "must": [
                            {
                                "term": {
                                    "id": affiliationId
                                }
                            }, {
                                "nested": {
                                    "path": "pubs",
                                    "score_mode" : "sum",
                                    "query": {
                                                "bool": {
                                                    "should": [
                                                        {
                                                            "multi_match" : {
                                                                "query" : bigrams,
                                                                "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams"],
                                                                "boost" : 2
                                                            }
                                                        },
                                                        {
                                                            "multi_match" : {
                                                                "query" : bigrams,
                                                                "fields" : ["pubs.abstract_en.bigrams", "pubs.abstract_fr.bigrams"]
                                                            }
                                                        },
                                                        bigrams_clean_fr,
                                                        bigrams_clean_en,
                                                        {
                                                            "multi_match" : {
                                                                "query" : trigrams,
                                                                "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams"],
                                                                "boost" : 2
                                                            }
                                                        },
                                                        {
                                                            "multi_match" : {
                                                                "query" : trigrams,
                                                                "fields" : ["pubs.abstract_en.trigrams", "pubs.abstract_fr.trigrams"]
                                                            }
                                                        },
                                                        trigrams_clean_fr,
                                                        trigrams_clean_en,
                                                        {
                                                            "multi_match" : {
                                                                "query" : text,
                                                                "fields" : ["pubs.title_en", "pubs.title_fr"],
                                                                "boost" : 2
                                                            }
                                                        },
                                                        {
                                                            "multi_match" : {
                                                                "query" : text,
                                                                "fields" : ["pubs.abstract_en", "pubs.abstract_fr", "pubs.keywords_en", "pubs.keywords_fr"]
                                                            }
                                                        }
                                                    ]
                                                }
                                        },
                                    "inner_hits": {
                                        "explain": true
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        }
    )}
    getAffiliationsWithTrigramTextExplain(affiliationId, trigrams, text){
        let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let trigrams_clean_en = [];
        let trigrams_clean_fr = [];
        let newtexttri = trigrams.match(re_tri);
        for(var i = 0; i < newtexttri.length; i++){
            trigrams_clean_en[i] = {
                    "match_phrase" : {
                    "pubs.keywords_en" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
            trigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "pubs.keywords_fr" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
        }
        return this.client.search({
            index: this.globals.index_team,
            type: this.globals.doc_type_team,
            body: {
                "query": {
                    "bool": {
                        "must": [
                            {
                                "term": {
                                    "id": affiliationId
                                }
                            }, {
                                "nested": {
                                    "path": "pubs",
                                    "score_mode" : "sum",
                                    "query": {
                                                "bool": {
                                                    "should": [
                                                        {
                                                            "multi_match" : {
                                                                "query" : trigrams,
                                                                "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams"],
                                                                "boost" : 2
                                                            }
                                                        },
                                                        {
                                                            "multi_match" : {
                                                                "query" : trigrams,
                                                                "fields" : ["pubs.abstract_en.trigrams", "pubs.abstract_fr.trigrams"]
                                                            }
                                                        },
                                                        trigrams_clean_fr,
                                                        trigrams_clean_en,
                                                        {
                                                            "multi_match" : {
                                                                "query" : text,
                                                                "fields" : ["pubs.title_en", "pubs.title_fr"],
                                                                "boost" : 2
                                                            }
                                                        },
                                                        {
                                                            "multi_match" : {
                                                                "query" : text,
                                                                "fields" : ["pubs.abstract_en", "pubs.abstract_fr", "pubs.keywords_en",  "pubs.keywords_fr"]
                                                            }
                                                        }
                                                    ]
                                                }
                                        },
                                    "inner_hits": {
                                        "explain": true
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        }
    )}
    getAffiliationsWithBigramTextExplain(affiliationId, bigrams, text){
        let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let bigrams_clean_en = [];
        let bigrams_clean_fr = [];
        let newtext = bigrams.match(re_bi);
        for(var i = 0; i < newtext.length; i++){
            bigrams_clean_en[i] = {
                    "match_phrase" : {
                    "pubs.keywords_en" : {
                        "query" : newtext[i],
                        "boost" : 2
                    }
                }
            };
            bigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "pubs.keywords_fr" : {
                        "query" : newtext[i],
                        "boost" : 2
                    }
                }
            };
        }
        return this.client.search({
            index: this.globals.index_team,
            type: this.globals.doc_type_team,
            body: {
                "query": {
                    "bool": {
                        "must": [
                            {
                                "term": {
                                    "id": affiliationId
                                }
                            }, {
                                "nested": {
                                    "path": "pubs",
                                    "score_mode" : "sum",
                                    "query": {
                                                "bool": {
                                                    "should": [
                                                        {
                                                            "multi_match" : {
                                                                "query" : bigrams,
                                                                "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams"],
                                                                "boost" : 2
                                                            }
                                                        },
                                                        {
                                                            "multi_match" : {
                                                                "query" : bigrams,
                                                                "fields" : ["pubs.abstract_en.bigrams", "pubs.abstract_fr.bigrams"]
                                                            }
                                                        },
                                                        bigrams_clean_fr,
                                                        bigrams_clean_en,
                                                        {
                                                            "multi_match" : {
                                                                "query" : text,
                                                                "fields" : ["pubs.title_en", "pubs.title_fr"],
                                                                "boost" : 2
                                                            }
                                                        },
                                                        {
                                                            "multi_match" : {
                                                                "query" : text,
                                                                "fields" : ["pubs.abstract_en", "pubs.abstract_fr", "pubs.keywords_en", "pubs.keywords_fr"]
                                                            }
                                                        }
                                                    ]
                                                }
                                        },
                                    "inner_hits": {
                                        "explain": true
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        }
    )}
    getAffiliationsWithBigramExplain(affiliationId, bigrams){
        let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let bigrams_clean_en = [];
        let bigrams_clean_fr = [];
        let newtext = bigrams.match(re_bi);
        for(var i = 0; i < newtext.length; i++){
            bigrams_clean_en[i] = {
                    "match_phrase" : {
                    "pubs.keywords_en" : {
                        "query" : newtext[i],
                        "boost" : 2
                    }
                }
            };
            bigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "pubs.keywords_fr" : {
                        "query" : newtext[i],
                        "boost" : 2
                    }
                }
            };
        }
        return this.client.search({
            index: this.globals.index_team,
            type: this.globals.doc_type_team,
            body: {
                "query": {
                    "bool": {
                        "must": [
                            {
                                "term": {
                                    "id": affiliationId
                                }
                            }, {
                                "nested": {
                                    "path": "pubs",
                                    "score_mode" : "sum",
                                    "query": {
                                                "bool": {
                                                    "should": [
                                                        {
                                                            "multi_match" : {
                                                                "query" : bigrams,
                                                                "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams"],
                                                                "boost" : 2
                                                            }
                                                        },
                                                        {
                                                            "multi_match" : {
                                                                "query" : bigrams,
                                                                "fields" : ["pubs.abstract_en.bigrams", "pubs.abstract_fr.bigrams"]
                                                            }
                                                        },
                                                        bigrams_clean_fr,
                                                        bigrams_clean_en
                                                    ]
                                                }
                                        },
                                    "inner_hits": {
                                        "explain": true
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        }
    )}
    getAffiliationsWithTrigramExplain(affiliationId, trigrams){
        let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let trigrams_clean_en = [];
        let trigrams_clean_fr = [];
        let newtexttri = trigrams.match(re_tri);
        for(var i = 0; i < newtexttri.length; i++){
            trigrams_clean_en[i] = {
                    "match_phrase" : {
                    "pubs.keywords_en" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
            trigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "pubs.keywords_fr" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
        }
        return this.client.search({
            index: this.globals.index_team,
            type: this.globals.doc_type_team,
            body: {
                "query": {
                    "bool": {
                        "must": [
                            {
                                "term": {
                                    "id": affiliationId
                                }
                            }, {
                                "nested": {
                                    "path": "pubs",
                                    "score_mode" : "sum",
                                    "query": {
                                                "bool": {
                                                    "should": [
                                                        {
                                                            "multi_match" : {
                                                                "query" : trigrams,
                                                                "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams"],
                                                                "boost" : 2
                                                            }
                                                        },
                                                        {
                                                            "multi_match" : {
                                                                "query" : trigrams,
                                                                "fields" : ["pubs.abstract_en.trigrams", "pubs.abstract_fr.trigrams"]
                                                            }
                                                        },
                                                        trigrams_clean_fr,
                                                        trigrams_clean_en
                                                    ]
                                                }
                                        },
                                    "inner_hits": {
                                        "explain": true
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        }
    )}
    getAffiliationsWithBigramKeywords(from, size, bigrams, keyword){
        var bigrams_clean = [];
        var trigrams = [];
        for(var i = 0; i < bigrams.length; i++){
            bigrams_clean[i] = {
                "multi_match": {
                    "query" : bigrams[i],
                    "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams"],
                    "type":       "most_fields",
                    "boost" : 50
                }
            };
            trigrams[i] = {
                "multi_match": {
                    "query" : bigrams[i],
                    "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams"],
                    "type":       "most_fields",
                    "boost" : 100
                }
            };
        }
        return this.client.search({
            index: this.globals.index_team,
            type: this.globals.doc_type_team,
            body: {
                "from": from,
                "size": size,
                "query": {
                    "nested": {
                        "path": "pubs",
                        "score_mode" : "sum",
                        "query": {
                            "function_score" : {
                                "script_score" : {
                                    "script" : {
                                        "lang": "painless",
                                        "inline": "_score * _score * _score * _score"
                                    }
                                },
                                "boost_mode":"multiply",
                                "query" : {
                                    "bool": {
                                        "should": [
                                            bigrams_clean,
                                            trigrams
                                        ],
                                        "must" : [
                                            {"match" :{
                                                "pubs.title_abstract" : keyword
                                            }}
                                        ]
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    )}

    getAffiliationsWithBigramKeywordsExplain(affiliationId, bigrams, keyword){
        var bigrams_clean = [];
        var trigrams = [];
        for(var i = 0; i < bigrams.length; i++){
            bigrams_clean[i] = {
                "multi_match": {
                    "query" : bigrams[i],
                    "fields" : ["pubs.title_en.bigrams", "pubs.title_fr.bigrams"],
                    "type":       "most_fields",
                    "boost" : 50
                }
            };
            trigrams[i] = {
                "multi_match": {
                    "query" : bigrams[i],
                    "fields" : ["pubs.title_en.trigrams", "pubs.title_fr.trigrams"],
                    "type":       "most_fields",
                    "boost" : 100
                }
            };
        }
        return this.client.search({
            index: this.globals.index_team,
            type: this.globals.doc_type_team,
            body: {
                "query": {
                    "bool": {
                        "must": [
                            {
                                "term": {
                                    "id": affiliationId
                                }
                            }, {
                                "nested": {
                                    "path": "pubs",

                                    "query": {
                                        "bool": {
                                            "should": [
                                                bigrams_clean,
                                                trigrams
                                            ],
                                            "must": [
                                                {
                                                    "match": {
                                                        "pubs.title_abstract": keyword
                                                    }
                                                }
                                            ]
                                        }
                                    },
                                    "inner_hits": {
                                        "explain": true
                                    }
                                }
                            }

                        ]
                    }
                }
            }
        }
    )}
    //Get the explanation for a user research Keywords on a affiliations
    getAffiliationExplain(affiliationId, textSearch){
        return this.client.search({
            index: this.globals.index_team,
            type: this.globals.doc_type_team,
            body: {
                size : 1,
                "query" : {
                    "bool": {
                        "must": [
                            {
                                "term": {
                                    "id": affiliationId
                                }
                            },
                            {
                                "nested": {
                                    "path": "pubs",
                                    "query": {
                                        "bool": {
                                            "should": [
                                                {
                                                    "multi_match": {
                                                        "fields": [
                                                            "pubs.abstract_en",
                                                            "pubs.abstract_fr"
                                                        ],
                                                        "query": textSearch
                                                    }
                                                },
                                                {
                                                    "multi_match": {
                                                        "fields": [
                                                            "pubs.title_en",
                                                            "pubs.title_fr"
                                                        ],
                                                        "query": textSearch
                                                    }
                                                },
                                                {
                                                    "multi_match": {
                                                        "query": textSearch,
                                                        "fields": ["pubs.keywords_*"]
                                                    }
                                                },
                                                {
                                                    "multi_match": {
                                                        "query": textSearch,
                                                        "fields": ["pubs.title_en.bigrams", "pubs.title_fr.bigrams"]
                                                    }
                                                },
                                                {
                                                    "multi_match": {
                                                        "query": textSearch,
                                                        "fields": ["pubs.title_en.trigrams", "pubs.title_en.trigrams"]
                                                    }
                                                }
                                            ]
                                        }
                                    },
                                    "inner_hits" : {
                                        "explain" : true
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        })
    }
    getTeamByIds(ids){
        return this.client.search({
            index : this.globals.index_team,
            type :this.globals.doc_type_team,
            body : {
                "query": {
                        "ids" : {
                        "values" : ids
                    }
                }
            }
        })
    }
    // Store comment in elasticsearch when a user make a comment on affiliation
    setCommentAff(affiliationId, name, comment){
            return this.client.create({
            index : 'comment',
            type : 'team_comment',
            body : {
                teamId : affiliationId,
                name : name,
                comment : comment
            }
        })
    }
}
