#!/bin/bash

npm install
npm run build:aot
mkdir -p aot/app
cp -r src/app/resources aot/app/resources
cp index.html aot/index.html
./node_modules/.bin/uglifycss src/app/css/custom-material.css src/app/css/custom.css src/app/css/helpers.css src/app/css/mobile-app.css src/app/css/list-result.css > aot/newallcss.bundle.css
cp favicon.ico aot/favicon.ico
cp src/app/css/angular-inria.css aot/angular-inria.css
cp zone.min.js aot/zone.min.js
cp shim.min.js aot/shim.min.js
cp -R src/app/css/Web_Sans aot/Web_Sans
cp -R src/app/css/Web_Serif aot/Web_Serif
cp -R src/app/lookinlabs-php aot/lookinlabs-php
# sed -i 's/compiler-cli": "^5.2.10"/compiler-cli": "^4.4.7"/g' package.json
cd aot/dist
sed -i 's/d3Cloud_1(/d3Cloud().size(/g' build.js
#sed -i 's/pie()/d3.pie()/g' build.js
#sed -i 's/= arc()/= d3.arc()/g' build.js
#sed -i 's/simulation(/d3.forceSimulation(/g' build.js
#sed -i 's/"link", link(links)/"link", d3.forceLink(links)/g' build.js
#sed -i 's/Buffer.TYPED_ARRAY_SUPPORT = global.TYPED_ARRAY_SUPPORT !== undefined//g' build.js
#sed -i 's/? global.TYPED_ARRAY_SUPPORT//g' build.js
#sed -i 's/: typedArraySupport();//g' build.js
