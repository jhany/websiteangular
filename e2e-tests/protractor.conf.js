exports.config = {
    allScriptsTimeout: 11000,

    specs: [
        'scenario-first.js',
        'scenarios-elastic-user.js',
        'scenarios-elastic-publication.js',
        'scenarios-elastic-affiliation.js',
        'scenario-social-graph.js'
    ],

    capabilities: {
        'browserName': 'chrome',
        chromeOptions: {
        args: ['--window-size=1700,800'] // THIS!
    }
    },

    baseUrl: 'https://newfeature.cominlabs.u-bretagneloire.fr/',

    framework: 'jasmine',

    jasmineNodeOpts: {
        defaultTimeoutInterval: 30000
    }
};
