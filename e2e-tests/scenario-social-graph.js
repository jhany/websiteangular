'use strict';
/**
 * Created by jhany on 11/21/16.
 */

describe('Similar', function () {

    describe('Team', function() {
        beforeEach(function () {
            browser.get('/teams-similar/struct-2539');
            browser.sleep(1000);
        });

        it('Show explain', function () {
            element.all(by.tagName('line')).get(0).click();
            browser.sleep(3000);
            expect(element(by.tagName('word-cloud-dir'))).toBeDefined();
        });

        it('Go to Team', function () {
            browser.actions().doubleClick(element.all(by.tagName('image')).get(5)).perform();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("/team/*");
                browser.ignoreSynchronization = false;
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });
    });

    describe('User Social Graph', function() {
      browser.get('');
      var textSearch = element(by.id("mat-input-0"));
      textSearch.clear();
      textSearch.sendKeys('cloud computing');
      browser.sleep(200);
      element.all(by.id('button-search')).click();
      browser.sleep(200);
      var user_id = "";
      var new_el = element.all(by.id('go-to-profile-user')).get(1).getAttribute('href').then(function(hrefValue){
        user_id = hrefValue;
        var user_id_tab = user_id.split('/');
        user_id = user_id_tab[user_id_tab.length - 1];
      });
        beforeEach(function () {

            browser.get('/individuals-relations/'+user_id);
            browser.sleep(600);
        });

        it('Show explain', function () {
            element.all(by.tagName('line')).get(0).click();
            browser.sleep(2000);
            expect(element(by.tagName('word-cloud'))).toBeDefined();
        });

        it('Go to Team', function () {
            browser.actions().doubleClick(element.all(by.tagName('image')).get(5)).perform();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("/individual/*");
                browser.ignoreSynchronization = false;
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });
    });

    describe('User', function() {

      browser.get('');
      var textSearch = element(by.id("mat-input-0"));
      textSearch.clear();
      textSearch.sendKeys('cloud computing');
      browser.sleep(200);
      element.all(by.id('button-search')).click();
      browser.sleep(200);
      var user_id = "";
      var new_el = element.all(by.id('go-to-profile-user')).get(1).getAttribute('href').then(function(hrefValue){
        user_id = hrefValue;
        var user_id_tab = user_id.split('/');
        user_id = user_id_tab[user_id_tab.length - 1];
      });

        beforeEach(function () {
            browser.get('/individuals-similars/'+user_id);
            browser.sleep(1000);
        });

        it('Should render similars users in a new tab', function () {
            element.all(by.id('button-similars')).get(0).click();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.sleep(1000);
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("/individuals-similars/*");
                browser.ignoreSynchronization = false;
                var nbUser = element.all(by.id('list-card'));
                expect(nbUser.count()).toBeGreaterThan(1);
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });

        it('Should open a dialog explanation', function () {
            element.all(by.id('btn-user-why')).get(0).click();
            expect(element(by.tagName('word-cloud-dir'))).toBeDefined();
        });

        it('Should open a new tab user relations', function () {
            element.all(by.id('button-people')).get(0).click();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.sleep(400);
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("/individuals-relations/*");
                browser.ignoreSynchronization = false;
                expect(element(by.tagName('svg'))).toBeDefined();
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });

        it('Should open a new tab profile', function () {
            element.all(by.id('go-to-profile-user')).get(0).click();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("/individual/*");
                browser.ignoreSynchronization = false;
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });
    });

    describe('User Team', function() {
        beforeEach(function () {
            browser.get('/individuals-team-keyword/struct-2539/cloud computing');
            browser.sleep(1000);
        });

        it('Should render similars users in a new tab', function () {
            element.all(by.id('button-similars')).get(0).click();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.sleep(1000);
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("/individuals-similars/*");
                browser.ignoreSynchronization = false;
                var nbUser = element.all(by.id('list-card'));
                expect(nbUser.count()).toBeGreaterThan(1);
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });

        it('Should render publication of user keywords', function () {
            var nbPub = element.all(by.id('nb-publication-keywords')).get(0);
            var nbPubText = nbPub.getText();
            element.all(by.id('publication-keywords')).get(0).click();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.sleep(1000);
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("/publications-individual-keyword/*/*");
                browser.ignoreSynchronization = false;
                var pubText = element(by.id('total-hit')).getText();
                expect(pubText).toEqual(nbPubText);
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });

        it('Should open a dialog explanation', function () {
            element.all(by.id('btn-user-why')).get(0).click();
            expect(element(by.tagName('word-cloud-dir'))).toBeDefined();
        });

        it('Should open a new tab user relations', function () {
            element.all(by.id('button-people')).get(0).click();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.sleep(400);
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("/individuals-relations/*");
                browser.ignoreSynchronization = false;
                expect(element(by.tagName('svg'))).toBeDefined();
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });

        it('Should open a new tab profile', function () {
            element.all(by.id('go-to-profile-user')).get(0).click();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("/individual/*");
                browser.ignoreSynchronization = false;
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });
    });
});
