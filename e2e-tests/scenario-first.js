'use strict';
/**
 * Created by jhany on 11/21/16.
 */

describe('Test', function () {

    describe('Base Fonctionality', function() {
        beforeEach(function () {
            browser.get('');
        });

        // it('Show explain', function () {
        //     browser.actions().
        //         mouseMove(element(by.id('show-explain'))).
        //         perform();
        //     var el = element(by.id('explain'));
        //     expect(el.getCssValue('visibility')).toBe('visible');;
        // });
        it('Cookies ok', function () {
            element.all(by.id('ok_cookie')).get(0).click();
        });

        it('Go to basket', function () {
            element.all(by.id('basket')).get(0).click();
            browser.sleep(200);
            expect(browser.getCurrentUrl()).toMatch("/basket");
        });

        it('Go to primer', function () {
            element(by.partialLinkText('About')).click();
            browser.sleep(200);
            expect(browser.getCurrentUrl()).toMatch("/primer");
        });

        it('Go to basket and come back', function () {
            element.all(by.id('basket')).get(0).click();
            browser.sleep(200);
            element.all(by.id('show-explain')).get(0).click();
            expect(browser.getCurrentUrl()).toMatch("/search");
        });

        // it('Try search key Enter', function () {
        //     var textSearch = element(by.id("mat-input-0"));
        //     textSearch.clear();
        //     textSearch.sendKeys('cloud computing');
        //     browser.actions().sendKeys(protractor.Key.ENTER).perform();
        //     browser.sleep(200);
        //     var list = element.all(by.id('list-aff-item'));
        //     expect(list.count()).toMatch("30");
        //     var list = element.all(by.id('publication-display'));
        //     expect(list.count()).toMatch("20");
        //     var list = element.all(by.id('list-card'));
        //     expect(list.count()).toMatch("30");
        // });

        it('Try search click button', function () {
            var textSearch = element(by.id("mat-input-0"));
            textSearch.clear();
            textSearch.sendKeys('cloud computing');
            browser.sleep(200);
            element.all(by.id('button-search')).click();
            browser.sleep(200);
            var list = element.all(by.id('list-aff-item'));
            expect(list.count()).toMatch("30");
            var list = element.all(by.id('publication-display'));
            expect(list.count()).toMatch("20");
            var list = element.all(by.id('list-card'));
            expect(list.count()).toMatch("30");
        });

        it('Try search and show did you mean', function () {
            var textSearch = element(by.id("mat-input-0"));
            textSearch.clear();
            textSearch.sendKeys('cloud computin');
            browser.sleep(200);
            element.all(by.id('button-search')).click();
            browser.sleep(200);
            var el = element.all(by.tagName('h3')).get(0);
            expect(el.getText()).toContain('Did you mean');
            expect(el.getCssValue('visibility')).toBe('visible');
        });

        it('Try search and show related topic', function () {
            var textSearch = element(by.id("mat-input-0"));
            textSearch.clear();
            textSearch.sendKeys('cloud computing');
            browser.sleep(200);
            element.all(by.id('button-search')).click();
            browser.sleep(200);
            var el = element.all(by.tagName('h3')).get(0);
            expect(el.getText()).toContain('Related');
            expect(el.getCssValue('visibility')).toBe('visible');
        });

        it('Try search indivdual', function () {
            var textSearch = element(by.id("mat-input-0"));
            textSearch.clear();
            textSearch.sendKeys('albert benveniste');
            browser.sleep(200);
            element.all(by.id('button-search')).click();
            browser.sleep(200);
            var list = element.all(by.id('list-card'));
            expect(list.count()).toMatch("1");
        });

        it('Should change page', function () {
            var textSearch = element(by.id("mat-input-0"));
            textSearch.clear();
            textSearch.sendKeys('cloud computing');
            browser.sleep(200);
            element.all(by.id('button-search')).click();
            browser.sleep(200);
            var el = element(by.id('mat-tab-label-0-1'));
            el.click();
            expect(el.getAttribute('aria-selected')).toBe('true');
            var el = element(by.id('mat-tab-label-2-2'));
            el.click();
            expect(el.getAttribute('aria-selected')).toBe('true');
            var el = element(by.id('mat-tab-label-1-3'));
            el.click();
            expect(el.getAttribute('aria-selected')).toBe('true');
        });

        it('click on check box Team', function () {
            var textSearch = element(by.id("mat-input-0"));
            textSearch.clear();
            textSearch.sendKeys('cloud computing');
            browser.sleep(200);
            element.all(by.id('button-search')).click();
            browser.sleep(200);
            var checkbox = element.all(by.tagName('mat-checkbox')).get(0).click();
            expect(element.all(by.id('list-aff-item')).count()).toMatch("0");;
        });

        it('click on check box Individual', function () {
            var textSearch = element(by.id("mat-input-0"));
            textSearch.clear();
            textSearch.sendKeys('cloud computing');
            browser.sleep(200);
            element.all(by.id('button-search')).click();
            browser.sleep(200);
            var checkbox = element.all(by.tagName('mat-checkbox')).get(1).click();
            expect(element.all(by.id('list-card')).count()).toMatch("0");;
        });

        it('click on check box publciation', function () {
            var textSearch = element(by.id("mat-input-0"));
            textSearch.clear();
            textSearch.sendKeys('cloud computing');
            browser.sleep(200);
            element.all(by.id('button-search')).click();
            browser.sleep(200);
            var checkbox = element.all(by.tagName('mat-checkbox')).get(2).click();
            expect(element.all(by.id('publication-display')).count()).toMatch("0");;
        });

        it('click on check box individual', function () {
            var textSearch = element(by.id("mat-input-0"));
            textSearch.clear();
            textSearch.sendKeys('cloud computing');
            browser.sleep(200);
            element.all(by.id('button-search')).click();
            browser.sleep(200);
            element.all(by.tagName('mat-checkbox')).get(1).click();
            var checkbox = element.all(by.tagName('mat-checkbox')).get(1).click();
            expect(element.all(by.id('list-card')).count()).toMatch("30");;
        });

        it('click on check box individual', function () {
            var textSearch = element(by.id("mat-input-0"));
            textSearch.clear();
            textSearch.sendKeys('cloud computing');
            browser.sleep(200);
            element.all(by.id('button-search')).click();
            browser.sleep(200);
            element.all(by.tagName('mat-checkbox')).get(0).click();
            var checkbox = element.all(by.tagName('mat-checkbox')).get(0).click();
            expect(element.all(by.id('list-aff-item')).count()).toMatch("30");;
        });

        it('click on check box publciation', function () {
            var textSearch = element(by.id("mat-input-0"));
            textSearch.clear();
            textSearch.sendKeys('cloud computing');
            browser.sleep(200);
            element.all(by.id('button-search')).click();
            browser.sleep(200);
            element.all(by.tagName('mat-checkbox')).get(2).click();
            var checkbox = element.all(by.tagName('mat-checkbox')).get(2).click();
            expect(element.all(by.id('publication-display')).count()).toMatch("20");;
        });

    });

});
